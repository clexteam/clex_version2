<div class="footer">
    <div class="footer-inner">
        <div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">CLEX</span>
							 &copy; 2007-2017, All rights reserved.
						</span>

        </div>
    </div>
</div>

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?=ASSETS ?>js/jquery-2.1.4.min.js"></script>
<script src="<?=ASSETS ?>js/sweetalert.min.js"></script>
<!-- <![endif]-->

<!--[if IE]>
<script src="<?=ASSETS ?>js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='<?=ASSETS ?>js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?=ASSETS ?>js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="<?=ASSETS ?>js/jquery-ui.custom.min.js"></script>

<script src="<?=ASSETS ?>js/chosen.jquery.min.js"></script>

<!-- ace scripts -->
<script src="<?=ASSETS ?>js/ace-elements.min.js"></script>
<script src="<?=ASSETS ?>js/ace.min.js"></script>
<script src="<?=ASSETS ?>js/moment.min.js"></script>
<script src="<?=ASSETS ?>js/bootstrap-datetimepicker.min.js"></script>

<!-- inline scripts related to this page -->
</body>
</html>

<script>
    // Uses a sweetalert to ask for user confirmation that they want to delete something
    function alertDelete(ajaxURL, message) {
        var targetURL = "<?= site_url(); ?>" + ajaxURL;

        swal({
                title: "Alert!",
                text: message,
                type: "warning",
                allowOutsideClick: true,
                showCancelButton: true,
                confirmButtonColor: "#C9302C",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false
            },
            function () {
                window.location.href = targetURL;
            }
        );
    }
    </script>