<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Clexonline</title>

    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?=ASSETS ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=ASSETS ?>font-awesome/4.5.0/css/font-awesome.min.css" />

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="<?=ASSETS ?>css/fonts.googleapis.com.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?=ASSETS ?>css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="<?=ASSETS ?>css/ace-part2.min.css" class="ace-main-stylesheet" />
    <![endif]-->
    <link rel="stylesheet" href="<?=ASSETS ?>css/ace-skins.min.css" />
    <link rel="stylesheet" href="<?=ASSETS ?>css/ace-rtl.min.css" />

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="<?=ASSETS ?>css/ace-ie.min.css" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="<?=ASSETS ?>js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="<?=ASSETS ?>js/html5shiv.min.js"></script>
    <script src="<?=ASSETS ?>js/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="<?=ASSETS ?>css/jquery-ui.custom.min.css" />
    <link rel="stylesheet" href="<?=ASSETS ?>css/chosen.min.css" />
    <link rel="stylesheet" href="<?=ASSETS ?>css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="<?=ASSETS ?>css/bootstrap-timepicker.min.css" />
    <link rel="stylesheet" href="<?=ASSETS ?>css/daterangepicker.min.css" />
    <link rel="stylesheet" href="<?=ASSETS ?>css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="<?=ASSETS ?>css/bootstrap-colorpicker.min.css" />
    <link rel="stylesheet" href="<?=ASSETS ?>css/sweetalert.css" />
</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default          ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
            <a href="index.html" class="navbar-brand">
                <small>
                    <img src="<?=ASSETS ?>images/clex.png" style="width: 4%;"> CLEX Admin
                </small>
            </a>
        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">

                <li class="light-blue dropdown-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="<?=ASSETS ?>images/avatars/user.jpg" alt="Jason's Photo" />
                        <span class="user-info">
									<small>Welcome,</small>
									<?= $this->session->userdata("name")?>
								</span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li>
                            <a href="#">
                                <i class="ace-icon fa fa-cog"></i>
                                Settings
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="ace-icon fa fa-user"></i>
                                Change password
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="<?= site_url() . 'logout'?>">
                                <i class="ace-icon fa fa-power-off"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- /.navbar-container -->
</div>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
    </script>

    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
            try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

        <div class="sidebar-shortcuts" id="sidebar-shortcuts">
            <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                <button class="btn btn-success">
                    <i class="ace-icon fa fa-signal"></i>
                </button>

                <button class="btn btn-info">
                    <i class="ace-icon fa fa-pencil"></i>
                </button>

                <button class="btn btn-warning">
                    <i class="ace-icon fa fa-users"></i>
                </button>

                <button class="btn btn-danger">
                    <i class="ace-icon fa fa-cogs"></i>
                </button>
            </div>

            <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                <span class="btn btn-success"></span>

                <span class="btn btn-info"></span>

                <span class="btn btn-warning"></span>

                <span class="btn btn-danger"></span>
            </div>
        </div><!-- /.sidebar-shortcuts -->

        <ul class="nav nav-list">
            <li class="">
                <a href="index.html">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>

                <b class="arrow"></b>
            </li>



            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Shipments </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="<?=site_url() ?>shipments">
                            <i class="menu-icon fa fa-caret-right"></i>
                           Manage shipments
                        </a>

                        <a href="<?=site_url() ?>shipments/add">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Add shipment
                        </a>

                        <b class="arrow"></b>
                    </li>


                </ul>
            </li>

            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Statuses </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="<?=site_url() ?>statuses">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Manage statuses
                        </a>

                        <a href="<?=site_url() ?>statuses/add">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Add status
                        </a>

                        <b class="arrow"></b>
                    </li>


                </ul>
            </li>

            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Customers </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="<?=site_url() ?>customers">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Manage customers
                        </a>

                        <a href="<?=site_url() ?>customers/add">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Add customer
                        </a>

                        <b class="arrow"></b>
                    </li>


                </ul>
            </li>


            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Users </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="<?=site_url() ?>users">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Manage users
                        </a>

                        <a href="<?=site_url() ?>users/add">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Add user
                        </a>

                        <b class="arrow"></b>
                    </li>


                </ul>
            </li>

            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Consignees </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="<?=site_url() ?>consignees">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Manage Consignees
                        </a>

                        <a href="<?=site_url() ?>consignees/add">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Add Consignee
                        </a>

                        <a href="<?=site_url() ?>consignees/upload">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Upload Consignees
                        </a>

                        <b class="arrow"></b>
                    </li>


                </ul>
            </li>

            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> SMS </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="<?=site_url() ?>sms/send_sms">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Send SMS
                        </a>


                        <b class="arrow"></b>
                    </li>


                </ul>
            </li>
            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Reports </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="<?=site_url() ?>reports">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Reports
                        </a>

                        <a href="<?=site_url() ?>reports/driver">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Driver Reports
                        </a>

                        <a href="<?=site_url() ?>reports/city_report">
                            <i class="menu-icon fa fa-caret-right"></i>
                            City Reports
                        </a>

                        <a href="<?=site_url() ?>reports/city_pickup">
                            <i class="menu-icon fa fa-caret-right"></i>
                            City Pickup Reports
                        </a>

                        <a href="<?=site_url() ?>reports/courier_pickup">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Courier Pickup
                        </a>

                        <a href="<?=site_url() ?>reports/ofd">
                            <i class="menu-icon fa fa-caret-right"></i>
                            OFD
                        </a>

                        <a href="<?=site_url() ?>reports/courier_ofd">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Courier OFD
                        </a>

                        <a href="<?=site_url() ?>reports/courier_hal">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Courier HAL
                        </a>


                        <b class="arrow"></b>
                    </li>


                </ul>
            </li>
        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
