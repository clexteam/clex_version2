<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends CI_Controller {

    private $sms_username = "966533341581";
    private $sms_password = "viva_12345%";
    private $source = "CLEX";
    //private $destination = "00966583255757";
    private $destination = "00966583255757";
    private $message;

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();

		$this->load->model("sms_model");
    }

    public function send_sms()
    {
        if (isset($_POST['submit']))
        {
            $this->destination = htmlspecialchars(trim($_POST["tel"]));
            $this->message = htmlspecialchars(trim($_POST["sms"]));

            $result = $this->sendSMS($this->sms_username, $this->sms_password, $this->destination, $this->source, $this->message, 0);
            if ($result == 0 OR $result == 1)
            {

                $data["status"] = "<p class='error-msg'> SMS sent.</p>";
            }
            else
            {
                $data["status"] = "<p class='error-msg'> SMS didn't send!</p>";
            }

            $stringToPost = "mobile=".$this->sms_username."&password=".$this->sms_password."&numbers=".$this->destination."&sender=".$this->source."&msg=".$this->message."&applicationType=24";
            $this->sms_model->log_clex_sms($stringToPost, $result, $this->message, date("Y-m-d:H-i-s"), $this->session->userdata("id"), $this->destination);
        }

        $this->load->view("send_sms", $data);
    }
	
	public function index()
	{
        $this->message = "Issued today \n";
        $shipments = $this->sms_model->get_issued_today();
        foreach ($shipments as $shipment)
        {
            $this->message .= " ".$shipment['number_of']."  ".$shipment['city_name']." \n";
        }

        echo $this->sendSMS($this->sms_username, $this->sms_password, $this->destination, $this->source, $this->message, 0);
	}

    public function delivered_today()
    {
        $shipments = $this->sms_model->get_delivered_today();
        foreach ($shipments as $shipment)
        {
            $this->message .= " ".$shipment['number_of']."  ".$shipment['city_name']." \n";
        }

        echo $this->sendSMS($this->sms_username, $this->sms_password, $this->destination, $this->source, $this->message, 0);
    }

    public function ofd_pod()
    {
        $rcs_count = $this->sms_model->get_rcs_count();
        $pod_count = $this->sms_model->get_pod_count();
        $this->message = "Received in CLEX  ".$rcs_count.' .. ';
        $this->message = $this->message."POD  ".$pod_count.' .. ';

        echo $this->sendSMS($this->sms_username, $this->sms_password, $this->destination, $this->source, $this->message, 0);
    }

    private function sendSMS($userAccount, $passAccount, $numbers, $sender, $msg, $MsgID, $timeSend=0, $dateSend=0, $deleteKey=0, $viewResult=1)
    {
        $url = "http://www.mobily.ws/api/msgSend.php";
        $applicationType = "24";
        $sender = urlencode($sender);
        $domainName = $_SERVER['SERVER_NAME'];
        $stringToPost = "mobile=".$userAccount."&password=".$passAccount."&numbers=".$numbers."&sender=".$sender."&msg=".$msg."&timeSend=".$timeSend."&dateSend=".$dateSend."&applicationType=".$applicationType."&domainName=clexonline.com&msgId=".$MsgID."&deleteKey=".$deleteKey."&lang=3";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $stringToPost);
        $result = curl_exec($ch);

        return $result;
    }


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */