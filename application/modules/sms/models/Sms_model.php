<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }


    public function get_consignees_count()
    {
        $query = $this->db->get('consignees');
        return $query->num_rows();
    }

    public function get_issued_today()
    {
        $query = "select distinct(city_name),count(*) as number_of from
                    (
                    SELECT cons_city ,CASE 
                    WHEN LOCATE('jeddah',LOWER(cons_city))>0 THEN 'jeddah'  
                    WHEN LOCATE('khober',LOWER(cons_city))>0 ||  LOCATE('khobar',LOWER(cons_city))>0 THEN 'khober'  
                    WHEN LOCATE('riyadh',LOWER(cons_city))>0 ||  LOCATE('riyahd',LOWER(cons_city))>0  THEN 'riyadh'  
                    WHEN LOCATE('abha',LOWER(cons_city))>0 THEN 'abha'  
                    WHEN LOCATE('dammam',LOWER(cons_city))>0  ||
                    LOCATE('dammmam',LOWER(cons_city))>0  ||
                    LOCATE('damam',LOWER(cons_city))>0   THEN 'dammam'  
                    WHEN LOCATE('qassim',LOWER(cons_city))>0 THEN 'qassim'
                    WHEN LOCATE('madinah',LOWER(cons_city))>0 THEN 'madinah'
                    WHEN LOCATE('makkah',LOWER(cons_city))>0 THEN 'makkah'  
                    ELSE 'Other' END as city_name  FROM (
                    select  cons_city from shipments  WHERE DATE(`issue_datetime`) = DATE(NOW()) and cust_acc_id=30
                    ) as template  ) as template2 group by city_name ";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function get_rcs_count()
    {
        $date = date("Y-m-d");
        $yesterday_day = date('Y-m-d', strtotime('-1 day'));
        $query = "select  distinct(`id`) from  shipment_movements  where  `to_status`=10 and DATE(`change_time`)>= '$yesterday_day' and `change_time`<'$date 17:00:00'";
        $query = $this->db->query($query);
        return $query->num_rows();

    }

    public function get_pod_count()
    {
        $date = date("Y-m-d");
        $query = "select  distinct(`id`) from  shipment_movements  where  `to_status`=7 and `change_time`>= '$date 00:00:00'";
        $query = $this->db->query($query);
        return $query->num_rows();

    }

    public function log_sms($sms_id)
    {
        date_default_timezone_set('Asia/Riyadh');
        $this->db->insert('sms_sent', array('sms_id' => $sms_id, 'date_sent' => date("Y-m-d H:i:s")));

    }

    public function log_clex_sms($stringToPost, $response, $msg, $date_sent, $user_send, $destination)
    {
        $this->db->insert('clexsms_sent', array('url' => $stringToPost, 'server_reply' => intval($response), 'sms_message' => $msg
                            ,'date_sent' => $date_sent, 'user_send' => $user_send,'phonenumber'=> $destination));
    }

    public function get_delivered_today()
    {
        $query = "select distinct(city_name),count(*) as number_of from
                    (
                    SELECT cons_city ,CASE
                    WHEN LOCATE('jeddah',LOWER(cons_city))>0 THEN 'jeddah'
                    WHEN LOCATE('khober',LOWER(cons_city))>0 ||  LOCATE('khobar',LOWER(cons_city))>0 THEN 'khober'
                    WHEN LOCATE('riyadh',LOWER(cons_city))>0 ||  LOCATE('riyahd',LOWER(cons_city))>0  THEN 'riyadh'
                    WHEN LOCATE('abha',LOWER(cons_city))>0 THEN 'abha'
                    WHEN LOCATE('dammam',LOWER(cons_city))>0  ||
                    LOCATE('dammmam',LOWER(cons_city))>0  ||
                    LOCATE('damam',LOWER(cons_city))>0   THEN 'dammam'
                    WHEN LOCATE('qassim',LOWER(cons_city))>0 THEN 'qassim'
                    WHEN LOCATE('madinah',LOWER(cons_city))>0 THEN 'madinah'
                    WHEN LOCATE('makkah',LOWER(cons_city))>0 THEN 'makkah'
                    
                    ELSE 'Other' END as city_name  FROM
                                (
                                    select shipments.cons_city  from shipments ,
                                        (
                                        select  distinct(shipment_movements.shipment_code) as shipment_code_id
                                            from shipment_movements,shipments   
                                            WHERE DATE(`change_time`) = DATE(NOW())
                                            and shipments.cust_acc_id=30
                                            and  shipment_movements.shipment_code=shipments.shipment_code
                                                                    and  shipment_movements.to_status=7
                                            order by shipment_movements.shipment_code
                                    ) as template
                                    where
                                    template.shipment_code_id=shipments.shipment_code
                                )
                                    as template2
                    
                    
                                     )  as template3 group by city_name ";
        $query = $this->db->query($query);
        return $query->result_array();
    }


    public function get_consignee_by_id($id)
	{
		$this->db->where('consignee_id', $id);
		$this->db->from('consignees');
        $this->db->join('cities', 'consignees.city_id = cities.city_id', 'left');
        $query = $this->db->get();
        return $query->result_array();
	}

    public function get_consignees_search_count($query)
    {
        $this->db->like('consignee_name', $query);
        $query = $this->db->get('consignees');
        return $query->num_rows();
    }

	public function get_all($limit, $offset)
	{
		$this->db->order_by("created_at", 'desc');
		if ($this->session->userdata("customer") !=0)
		$this->db->where('consignees.cust_acc_id', $this->session->userdata("customer"));
        $this->db->join('customers', 'customers.customer_id = consignees.cust_acc_id', 'left');
        $this->db->join('cities', 'cities.city_id = consignees.city_id', 'left');
		$query = $this->db->get('consignees', $limit, $offset);
		return $query->result_array();
	}

    public function get_all_by_keyword($query, $limit, $offset)
    {
        $this->db->like('consignee_name', $query);
        $this->db->order_by("created_at", 'desc');
        $this->db->join('customers', 'customers.customer_id = consignees.cust_acc_id', 'left');
        $this->db->join('cities', 'cities.city_id = consignees.city_id', 'left');
        $query = $this->db->get('consignees', $limit, $offset);
        return $query->result_array();
    }

    public function get_shipment_by_id($id)
    {
        $this->db->where('shipment_code', $id);
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_bulk_shipments($bulk_codes, $limit, $offset)
    {
        $this->db->where("shipment_code IN ($bulk_codes)");
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $query = $this->db->get('shipments', $limit, $offset);
        return $query->result_array();
    }

    public function get_shipment_movements($id)
    {
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $this->db->where("shipment_movements.shipment_code", $id);
        $query = $this->db->get('shipment_movements');
        return $query->result_array();
    }

    public function insert_consignee($cons_no, $name, $type, $customer, $city, $district, $street, $address,
                                     $zip, $contact, $tel, $mob, $remarks, $is_active)
    {
       $this->db->insert('consignees', array('consignee_no' => $cons_no, 'consignee_name' => $name, 'type' => $type , 'cust_acc_id' => $customer, 'city_id' => $city,
           'district' => $district, 'street' => $street, 'address' => $address, 'zip' => $zip, 'contact' => $contact, 'telephone' => $tel,
           'mobile' => $mob, 'remarks' => $remarks, 'is_active' => $is_active));
       return $this->db->insert_id();

    }

    public function add_to_movement_history($id, $from_status, $to_status, $changed_by)
    {
        $this->db->insert('shipment_movements', array('shipment_code' => $id, 'from_status' => $from_status ,'to_status' => $to_status, 'change_time' => date("Y-m-d H:i:s"), 'changed_by' => $changed_by));

    }

    public function update_consignee($id, $code, $name, $type, $customer, $city, $district, $street, $address,
                                     $zip, $contact, $tel, $mob, $remarks, $is_active)
    {
        $this->db->where("consignee_id", $id);

        $this->db->update('consignees', array('consignee_name' => $name, 'consignee_code' => $code, 'type' => $type , 'cust_acc_id' => $customer, 'city_id' => $city,
            'district' => $district, 'street' => $street, 'address' => $address, 'zip' => $zip, 'contact' => $contact, 'telephone' => $tel,
            'mobile' => $mob, 'remarks' => $remarks, 'is_active' => $is_active));

    }

	public function edit_doctor($id, $name, $gender, $title, $section, $city, $area, $address, $phone, $work_time, $price, $describtion, $picture_name)
	{
		$this->db->where("id", $id);
		if (!empty($picture_name))
		{
			$this->db->update('doctors', array('name' => $name, 'gender' => $gender, 'title' => $title, 'section_id' => $section, 'city_id' => $city, 'area_id' => $area,
					'address' => $address, 'phone' => $phone, 'working_time' => $work_time, 'price' => $price, 'description' => $describtion, 'image' => $picture_name));
		}
		else
		{
			$this->db->update('doctors', array('name' => $name, 'gender' => $gender, 'title' => $title, 'section_id' => $section, 'city_id' => $city, 'area_id' => $area,
					'address' => $address, 'phone' => $phone, 'working_time' => $work_time, 'description' => $describtion, 'price' => $price));

		}

	}


    public function delete_old_consignees($customer_id)
    {
        $this->db->where("cust_acc_id", $customer_id);
        $this->db->delete('consignees');
    }

    public function get_city_by_name($city)
    {
        $this->db->where("city", $city);
        $query = $this->db->get('cities');
        return $query->row_array();
    }

    public function check_is_consignee_exits($cons_no, $cust_no)
    {
        $this->db->where('consignee_no', $cons_no);
        $this->db->where('cust_acc_id', $cust_no);
        $query = $this->db->get('consignees');
        return $query->num_rows();
    }


}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */