<?php $this->load->view("header"); ?>

<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>

                <li>
                    <a href="#">Users</a>
                </li>
                <li class="active">Add user</li>
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- /.nav-search -->
        </div>

        <div class="page-content">
            <div class="ace-settings-container" id="ace-settings-container">
                <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                    <i class="ace-icon fa fa-cog bigger-130"></i>
                </div>

                <div class="ace-settings-box clearfix" id="ace-settings-box">
                    <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <div class="pull-left">
                                <select id="skin-colorpicker" class="hide">
                                    <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                    <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                    <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                    <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                </select>
                            </div>
                            <span>&nbsp; Choose Skin</span>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                            <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                            <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                            <label class="lbl" for="ace-settings-add-container">
                                Inside
                                <b>.container</b>
                            </label>
                        </div>
                    </div><!-- /.pull-left -->

                    <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                            <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                            <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                            <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                        </div>
                    </div><!-- /.pull-left -->
                </div><!-- /.ace-settings-box -->
            </div><!-- /.ace-settings-container -->

            <div class="page-header">
                <h1>
                    Users
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                       Add user
                    </small>
                    <div style="color: red"><?php if(!empty($status)) echo $status; ?></div>

                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-9">
                    <!-- PAGE CONTENT BEGINS -->
                    <form class="form-horizontal" role="form" method="post">

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Full Name </label>

                            <div class="col-sm-6">
                                <input type="text" id="form-field-2" name="fullname" value="<?= @$_POST['fullname'] ?>" placeholder="Full Name.." class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Type </label>

                            <div class="col-sm-4">
                                <select class="form-control" class="col-xs-6 col-sm-5" id="form-field-select-4"  name="type" required>
                                    <option value="">Select Type  </option>
                                    <option value="0" <?php if(isset($_POST['type']) AND @$_POST['type'] == 0) echo "selected"?>>CLEX User </option>
                                    <option value="1" <?php if(isset($_POST['type']) AND @$_POST['type'] == 1) echo "selected"?>>Customer User</option>
                                    <option value="2" <?php if(isset($_POST['type']) AND @$_POST['type'] == 2) echo "selected"?>>CLEX Driver</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="customer_select">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Customer Account </label>

                            <div class="col-sm-4">
                                <select class="chosen-select form-control" class="col-xs-6 col-sm-5" id="form-field-select-3" data-placeholder="Choose a customer account..." name="customer" >
                                    <option value="">  </option>
                                    <?php foreach ($customers as $customer): ?>
                                        <option value="<?= $customer['customer_id']  ?>" <?php if(isset($_POST['customer']) AND $customer['customer_id'] == @$_POST['customer']) echo "selected"?> ><?= $customer['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>




                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Username </label>

                            <div class="col-sm-6">
                                <input type="text" id="form-field-2" name="username" value="<?= @$_POST['username'] ?>" placeholder="Username" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Email </label>

                            <div class="col-sm-6">
                                <input type="email" id="form-field-2" name="email" value="<?= @$_POST['email'] ?>" placeholder="Email" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Password </label>

                            <div class="col-sm-6">
                                <input type="password" id="password"  name="password" placeholder="Password" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Confirm Password </label>

                            <div class="col-sm-6">
                                <input type="password" id="confirm_pass" name="confirm_pass" placeholder="Confirm Password" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">City</label>

                        <div class="col-sm-4">
                            <select class="form-control" id="form-field-select-1" name="city" required>
                                <option value=""> Select City </option>
                                <?php foreach ($cities as $city): ?>
                                    <option value="<?= $city['city_id'] ?>" <?php if($city['city_id'] == @$_POST['city']) echo "selected"?>><?= $city['city'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-6">Mobile</label>
                            <div class="col-sm-6">
                                <input type="text" id="form-field-2" name="mob"  value="<?= @$_POST['mob'] ?>" placeholder="MOB" class="col-xs-10 col-sm-6"/>

                            </div>
                        </div>


                        <div class="space-4"></div>
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-6">Active</label>
                        <div class="checkbox">
                            <label>
                                <?php if (isset($_POST['active'])): ?>
                                    <input name="active" value="1" type="checkbox" class="ace" checked>
                                <?php else: ?>
                                    <input name="active" value="1" type="checkbox" class="ace">
                                <?php endif; ?>
                                <span class="lbl"> </span>
                            </label>
                        </div>
                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" name="submit" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>


                            </div>
                        </div>


                    </form>


                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<?php $this->load->view("footer"); ?> 
</body>
</html>
<script>

	$( "#form-field-select-4" ).change(function() {
	    if ($( "#form-field-select-4" ).val() == 1)
            $( "#customer_select" ).css('display', 'block');
	    else
            $( "#customer_select" ).css('display', 'none');
	});



if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true});
    //resize the chosen on window resize


    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
        if(event_name != 'sidebar_collapsed') return;
        $('.chosen-select').each(function() {
            var $this = $(this);
            $this.next().css({'width': $this.parent().width()});
        })
    });


}

    var password = document.getElementById("password")
        , confirm_password = document.getElementById("confirm_pass");

    function validatePassword(){
        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

</script>