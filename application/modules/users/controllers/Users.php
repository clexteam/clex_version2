<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("users_model");
    }
	
	
	public function index()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
			$current_page = (int) $this->uri->segment(2);
			$per_page = 10;
			$users_count = $this->common_model->get_table_rows_count('users');
			$config["base_url"] = site_url() . "users/";
			$config['uri_segment'] = 2;
			$config["total_rows"] = $users_count;
			$config["per_page"] = $per_page;

            //config for bootstrap pagination class integration
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			$users = $this->users_model->get_all($per_page, $current_page);
			if (isset($_POST['search']) AND !empty($_POST['search']))
			{
                redirect(site_url() . "users_search/".htmlspecialchars(trim($_POST['search'])));
            }

			if ($users)
			{
				$data["shipments"] = $users;
			}
			
			$this->load->view("manage_users_view", $data);
//		}
	}
	
	
	public function add()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
			$data['customers'] = $this->users_model->get_active_customers();
            $data['cities'] = $this->common_model->get_all_from_table("cities");

			if (isset($_POST["submit"]))
			{
                $fullname = htmlspecialchars(trim($_POST["fullname"]));
                $type = $_POST['type'];
                if ($_POST['customer'] != '') $customer = $_POST['customer']; else $customer = '';
                $username = htmlspecialchars(trim($_POST["username"]));
                $email = htmlspecialchars(trim($_POST["email"]));
                $password = htmlspecialchars(trim($_POST["password"]));
                $city = $_POST['city'];
                $mob = @htmlspecialchars(trim($_POST["mob"]));
                if (isset($_POST['active'])) $is_active = 1; else $is_active = 0;

                $username_exits = $this->common_model->get_subject_with_token("users", "username", $username);

                $data['status'] = "";

				if (empty($fullname) OR empty($username) OR empty($email) OR empty($password) OR empty($city))
				{
					$data["status"].= "<p class='error-msg'> Please fill in all required fields!</p>";
				}
				elseif ($type == 1 AND $customer == '')
                {
                    $data["status"].= "<p class='error-msg'> Please select the customer!</p>";
                }
                elseif (!empty($username_exits))
                {
                    $data["status"].= "<p class='error-msg'> This username already exits!</p>";
                }
				else
				{
                    $insert_id = $this->users_model->insert_user($fullname, $customer, $username, $email, $password, $city, $type, $mob, $is_active);
                    $this->session->set_flashdata("status", "User added successfully.");
                    redirect(site_url() . "users");
				}
			}
			
			$this->load->view("add_user_view", $data);
	//	}
	}

    public function search($search_keyword = "")
    {
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
        if (!$search_keyword) show_404();
        $search_keyword = htmlspecialchars_decode($search_keyword);
        $data = array();
        $current_page = (int) $this->uri->segment(3);
        $per_page = 10;
        $config["base_url"] = site_url() . "users_search/$search_keyword";
        $config['uri_segment'] = 3;
        $config["total_rows"] = $this->users_model->get_users_search_count($search_keyword);
        $config["per_page"] = $per_page;
        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
        $users = $this->users_model->get_all_by_keyword($search_keyword, $per_page, $current_page);

        if (isset($_POST['search']) AND !empty($_POST['search']))
        {
            redirect(site_url() . "users_search/".htmlspecialchars(trim($_POST['search'])));
        }

        if ($users)
        {
            $data["shipments"] = $users;
        }

        $this->load->view("manage_users_view", $data);
//		}
    }
	
	public function edit($id = "")
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
            $data['user'] = $this->common_model->get_subject_with_token("users", "id", $id);
            if (!$id OR !$data['user']) show_404();
			$data['cities'] = $this->common_model->get_all_from_table("cities");
            $data['customers'] = $this->users_model->get_active_customers();


            if (isset($_POST["submit"]))
            {
                $fullname = htmlspecialchars(trim($_POST["fullname"]));
                $type = $_POST['type'];
                if ($_POST['customer'] != '') $customer = $_POST['customer']; else $customer = '';
                $username = htmlspecialchars(trim($_POST["username"]));
                $email = htmlspecialchars(trim($_POST["email"]));
                $password = @htmlspecialchars(trim($_POST["password"]));
                $city = $_POST['city'];
                $mob = @htmlspecialchars(trim($_POST["mob"]));
                if (isset($_POST['active'])) $is_active = 1; else $is_active = 0;

                $username_exits = $this->common_model->get_subject_with_token("users", "username", $username);

                $data['status'] = "";

                if (empty($fullname) OR empty($username) OR empty($email) OR empty($city))
                {
                    $data["status"].= "<p class='error-msg'> Please fill in all required fields!</p>";
                }
                elseif ($type == 1 AND $customer == '')
                {
                    $data["status"].= "<p class='error-msg'> Please select the customer!</p>";
                }
                elseif (!empty($username_exits) AND $username_exits['id'] != $id)
                {
                    $data["status"].= "<p class='error-msg'> This username already exits!</p>";
                }
                else
                {
                    $insert_id = $this->users_model->update_user($id, $fullname, $customer, $username, $email, $password, $city, $type, $mob, $is_active, $data['user']['password']);
                    $this->session->set_flashdata("status", "User updated successfully.");
                    redirect(site_url() . "users");
                }
            }

        $this->load->view("edit_user_view", $data);
//		}
	}
	
	
	public function delete($id = "")
    {	
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$user = $this->common_model->get_subject_with_token("users", "id", $id);
			if (empty($id) OR ! $user) redirect(site_url() . "users");
		
			$this->common_model->delete_subject("users", "id", $id);
			
			$this->session->set_flashdata("status", "User deleted successfully!");
			redirect($_SERVER['HTTP_REFERER']);
//		}
	}
	


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */