<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }


    public function get_active_customers()
    {
        $this->db->where('is_active', 1);
        $query = $this->db->get('customers');
        return $query->result_array();
    }

	public function get_consignee_by_id($id)
	{
		$this->db->where('consignee_id', $id);
		$this->db->from('consignees');
        $this->db->join('cities', 'consignees.city_id = cities.city_id', 'left');
        $query = $this->db->get();
        return $query->result_array();
	}

    public function get_users_search_count($query)
    {
        $this->db->like('fullname', $query);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('users');
        return $query->num_rows();
    }

	public function get_all($limit, $offset)
	{
		$this->db->order_by("created_at", 'desc');
        $this->db->join('customers', 'customers.customer_id = users.customer_acc_id', 'left');
        $this->db->join('cities', 'cities.city_id = users.city_id', 'left');
		$query = $this->db->get('users', $limit, $offset);
		return $query->result_array();
	}

    public function get_all_by_keyword($query, $limit, $offset)
    {
        $this->db->like('fullname', $query);
        $this->db->order_by("created_at", 'desc');
        $this->db->join('customers', 'customers.customer_id = users.customer_acc_id', 'left');
        $this->db->join('cities', 'cities.city_id = users.city_id', 'left');
        $query = $this->db->get('users', $limit, $offset);
        return $query->result_array();
    }

    public function get_shipment_by_id($id)
    {
        $this->db->where('shipment_code', $id);
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_bulk_shipments($bulk_codes, $limit, $offset)
    {
        $this->db->where("shipment_code IN ($bulk_codes)");
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $query = $this->db->get('shipments', $limit, $offset);
        return $query->result_array();
    }

    public function get_shipment_movements($id)
    {
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $this->db->where("shipment_movements.shipment_code", $id);
        $query = $this->db->get('shipment_movements');
        return $query->result_array();
    }

    public function insert_user($fullname, $customer, $username, $email, $password, $city, $type, $mob, $is_active)
    {
        $password = password_hash($password, PASSWORD_DEFAULT);
        $this->db->insert('users', array('fullname' => $fullname, 'customer_acc_id' => $customer , 'email' => $email, 'username' => $username, 'password' => $password, 'type' => $type, 'city_id' => $city,
			   'user_mobile' => $mob, 'is_active' => $is_active));

    }

    public function add_to_movement_history($id, $from_status, $to_status, $changed_by)
    {
        $this->db->insert('shipment_movements', array('shipment_code' => $id, 'from_status' => $from_status ,'to_status' => $to_status, 'change_time' => date("Y-m-d H:i:s"), 'changed_by' => $changed_by));

    }

    public function update_user($id, $fullname, $customer, $username, $email, $password, $city, $type, $mob, $is_active, $old_password)
    {
        if (!empty($password)) $password = password_hash($password, PASSWORD_DEFAULT); else $password = $old_password;
        $this->db->where("id", $id);

        $this->db->update('users', array('fullname' => $fullname, 'customer_acc_id' => $customer , 'email' => $email, 'username' => $username, 'password' => $password, 'type' => $type, 'city_id' => $city,
            'user_mobile' => $mob, 'is_active' => $is_active));

    }

	public function edit_doctor($id, $name, $gender, $title, $section, $city, $area, $address, $phone, $work_time, $price, $describtion, $picture_name)
	{
		$this->db->where("id", $id);
		if (!empty($picture_name))
		{
			$this->db->update('doctors', array('name' => $name, 'gender' => $gender, 'title' => $title, 'section_id' => $section, 'city_id' => $city, 'area_id' => $area,
					'address' => $address, 'phone' => $phone, 'working_time' => $work_time, 'price' => $price, 'description' => $describtion, 'image' => $picture_name));
		}
		else
		{
			$this->db->update('doctors', array('name' => $name, 'gender' => $gender, 'title' => $title, 'section_id' => $section, 'city_id' => $city, 'area_id' => $area,
					'address' => $address, 'phone' => $phone, 'working_time' => $work_time, 'description' => $describtion, 'price' => $price));

		}

	}

    

	

}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */