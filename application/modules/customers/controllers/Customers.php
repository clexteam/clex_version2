<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("customers_model");
    }
	
	
	public function index()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
			$current_page = (int) $this->uri->segment(2);
			$per_page = 10;
			$shipments_count = $this->customers_model->shipments_count();
			$config["base_url"] = site_url() . "customers/";
			$config['uri_segment'] = 2;
			$config["total_rows"] = $shipments_count;
			$config["per_page"] = $per_page;

            //config for bootstrap pagination class integration
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			$shipments = $this->customers_model->get_all($per_page, $current_page);
			if (isset($_POST['search']) AND !empty($_POST['search']))
			{
                redirect(site_url() . "customers_search/".htmlspecialchars(trim($_POST['search'])));
            }

			if ($shipments)
			{
				$data["shipments"] = $shipments;
			}
			
			$this->load->view("manage_customers_view", $data);
//		}
	}
	
	
	public function add()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
            $data['cities'] = $this->common_model->get_all_from_table("cities");

			if (isset($_POST["submit"]))
			{
                $name = htmlspecialchars(trim($_POST["name"]));
                $company_name = htmlspecialchars(trim($_POST["company_name"]));
				$city = $_POST['city'];
				$district = htmlspecialchars(trim($_POST["district"]));
                $street = htmlspecialchars(trim($_POST["street"]));
                $zip = @htmlspecialchars(trim($_POST["zip"]));
                $contact = @htmlspecialchars(trim($_POST["contact"]));
                $tel = @htmlspecialchars(trim($_POST["tel"]));
                $mob = @htmlspecialchars(trim($_POST["mob"]));

				if (empty($name) OR empty($company_name) OR empty($city) OR empty($street) OR empty($contact) OR empty($tel))
				{
					$data["status"] = "<p class='error-msg'> Please fill in all required fields!</p>";
				}
				else
				{
                    $insert_id = $this->customers_model->insert_customer($name, $company_name, $city, $district, $street, $zip, $contact, $tel, $mob);
                    $this->session->set_flashdata("status", "Customer added successfully.");
                    redirect(site_url() . "customers");
				}
			}
			
			$this->load->view("add_customer_view", $data);
	//	}
	}


    public function search($search_keyword = "")
    {
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
        if (!$search_keyword) show_404();
        $search_keyword = htmlspecialchars_decode($search_keyword);
        $data = array();
        $current_page = (int) $this->uri->segment(3);
        $per_page = 1;
        $config["base_url"] = site_url() . "customers_search/$search_keyword";
        $config['uri_segment'] = 3;
        $config["total_rows"] = $this->customers_model->get_customers_search_count($search_keyword);
        $config["per_page"] = $per_page;
        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
        $customers = $this->customers_model->get_all_by_keyword($search_keyword, $per_page, $current_page);

        if (isset($_POST['search']) AND !empty($_POST['search']))
        {
            redirect(site_url() . "customers_search/".htmlspecialchars(trim($_POST['search'])));
        }

        if ($customers)
        {
            $data["shipments"] = $customers;
        }

        $this->load->view("manage_customers_view", $data);
//		}
    }

	
	public function edit($id = "")
	{
	    $data = array();
        $data['customer'] = $this->common_model->get_subject_with_token("customers", "customer_id", $id);
        if (!$id OR !$data['customer']) show_404();
        $data['cities'] = $this->common_model->get_all_from_table("cities");

        if (isset($_POST["submit"]))
        {
            $name = htmlspecialchars(trim($_POST["name"]));
            $company_name = htmlspecialchars(trim($_POST["company_name"]));
            $city = $_POST['city'];
            $district = htmlspecialchars(trim($_POST["district"]));
            $street = htmlspecialchars(trim($_POST["street"]));
            $zip = @htmlspecialchars(trim($_POST["zip"]));
            $contact = @htmlspecialchars(trim($_POST["contact"]));
            $tel = @htmlspecialchars(trim($_POST["tel"]));
            $mob = @htmlspecialchars(trim($_POST["mob"]));

            if (empty($name) OR empty($company_name) OR empty($city) OR empty($street) OR empty($contact) OR empty($tel))
            {
                $data["status"] = "<p class='error-msg'> Please fill in all required fields!</p>";
            }
            else
            {
                $insert_id = $this->customers_model->update_customer($id, $name, $company_name, $city, $district, $street, $zip, $contact, $tel, $mob);
                $this->session->set_flashdata("status", "Customer edited successfully.");
                redirect(site_url() . "customers");
            }
        }

        $this->load->view("edit_customer_view", $data);
	}
	
	
	public function delete($id = "")
    {	
		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
		if ($authorized)
		{
			$doctor = $this->common_model->get_subject_with_token("doctors", "id", $id);
			if (empty($id) OR ! $doctor) redirect(site_url() . "doctors");
		
			$this->common_model->delete_subject("doctors", "id", $id);
			
			$this->session->set_flashdata("status", "تمت العملية بنجاح");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */