<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
	
	public function get_active_sections()
	{
		$sql = "SELECT * FROM `sections` WHERE `active` = 1 ORDER BY `arrange_flag` ASC";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() >= 1)
		{			
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

    public function get_active_consignees()
    {
        $this->db->where('is_active', 1);
        $query = $this->db->get('consignees');
        return $query->result_array();
    }

	public function get_consignee_by_id($id)
	{
		$this->db->where('consignee_id', $id);
		$this->db->from('consignees');
        $this->db->join('cities', 'consignees.city_id = cities.city_id', 'left');
        $query = $this->db->get();
        return $query->result_array();
	}

    public function shipments_count()
    {
        $this->db->order_by('customer_id', 'desc');
        $query = $this->db->get('customers');
        return $query->num_rows();
    }

	public function get_all($limit, $offset)
	{
        $this->db->join('cities', 'cities.city_id = customers.city_id', 'left');
        $query = $this->db->get('customers', $limit, $offset);
		return $query->result_array();
	}



    public function insert_customer($name, $company_name, $city, $district, $street, $zip, $contact, $tel, $mob)
    {
       $this->db->insert('customers', array('name' => $name, 'company_name' => $company_name ,'district' => $district, 'street' => $street, 'city_id' => $city,
               'zip' => $zip, 'contact' => $contact, 'telephone' => $tel, 'mobile' => $mob));

    }


    public function get_customers_search_count($keyword)
    {
        /* Returns count of all rows in a table. Returns a number. */

        $sql = "SELECT COUNT(`customer_id`) AS `count` FROM `customers` WHERE name like '%$keyword%'";
        $query = $this->db->query($sql);
        $count = $query->row_array();
        return $count["count"];
    }

    public function update_customer($id, $name, $company_name, $city, $district, $street, $zip, $contact, $tel, $mob)
    {
        $this->db->where("customer_id", $id);

        $this->db->update('customers', array('name' => $name, 'company_name' => $company_name ,'district' => $district, 'street' => $street, 'city_id' => $city,
            'zip' => $zip, 'contact' => $contact, 'telephone' => $tel, 'mobile' => $mob));

    }

    public function get_all_by_keyword($keyword, $limit, $offset)
    {
        $this->db->join('cities', 'cities.city_id = customers.city_id', 'left');
        $this->db->like('name', $keyword);
        $query = $this->db->get('customers', $limit, $offset);
        return $query->result_array();
    }
	

}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */