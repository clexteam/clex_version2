<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statuses_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
	
	public function get_active_sections()
	{
		$sql = "SELECT * FROM `sections` WHERE `active` = 1 ORDER BY `arrange_flag` ASC";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() >= 1)
		{			
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

    public function get_active_consignees()
    {
        $this->db->where('is_active', 1);
        $query = $this->db->get('consignees');
        return $query->result_array();
    }

	public function get_consignee_by_id($id)
	{
		$this->db->where('consignee_id', $id);
		$this->db->from('consignees');
        $this->db->join('cities', 'consignees.city_id = cities.city_id', 'left');
        $query = $this->db->get();
        return $query->result_array();
	}

    public function shipments_count()
    {
        //$this->db->where('consignee_id', $id);
        $this->db->order_by('shipment_code', 'desc');
        $query = $this->db->get('shipments');
        return $query->num_rows();
    }

	public function get_all($limit, $offset)
	{
		$query = $this->db->get('statuses', $limit, $offset);
		return $query->result_array();
	}

	public function get_all_by_keyword($keyword, $limit, $offset)
	{
	    $this->db->like('title', $keyword);
		$query = $this->db->get('statuses', $limit, $offset);
		return $query->result_array();
	}



    public function get_shipment_by_id($id)
    {
        $this->db->where('shipment_code', $id);
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_statuses_search_count($keyword)
    {
        /* Returns count of all rows in a table. Returns a number. */

        $sql = "SELECT COUNT(`id`) AS `count` FROM `statuses` WHERE title like '%$keyword%'";
        $query = $this->db->query($sql);
        $count = $query->row_array();
        return $count["count"];
    }


    public function get_shipment_movements($id)
    {
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $this->db->where("shipment_movements.shipment_code", $id);
        $query = $this->db->get('shipment_movements');
        return $query->result_array();
    }

    public function insert_status($title, $details)
    {
       $this->db->insert('statuses', array('title' => $title, 'details' => $details));

    }

    public function update_status($id, $title, $details)
    {
        $this->db->where("id", $id);

        $this->db->update('statuses', array('title' => $title, 'details' => $details));

    }

	

}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */