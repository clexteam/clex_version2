<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statuses extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("statuses_model");
    }
	
	
	public function index()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
			$current_page = (int) $this->uri->segment(2);
			$per_page = 10;
			$shipments_count = $this->common_model->get_table_rows_count('statuses');
			$config["base_url"] = site_url() . "statuses/";
			$config['uri_segment'] = 2;
			$config["total_rows"] = $shipments_count;
			$config["per_page"] = $per_page;

            //config for bootstrap pagination class integration
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			$shipments = $this->statuses_model->get_all($per_page, $current_page);
			if (isset($_POST['search']) AND !empty($_POST['search']))
			{
                redirect(site_url() . "status_search/".htmlspecialchars(trim($_POST['search'])));
            }

			if ($shipments)
			{
				$data["shipments"] = $shipments;
			}
			else
            {
                show_404();
            }
			
			$this->load->view("manage_statuses_view", $data);
//		}
	}
	
	
	public function add()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();

			if (isset($_POST["submit"]))
			{
				$title = htmlspecialchars(trim($_POST["title"]));
                $details = htmlspecialchars(trim($_POST["details"]));

				if (empty($title))
				{
					$data["status"] = "<p class='error-msg'> Please fill in all required fields!</p>";
				}
				else
				{
                    $insert_id = $this->statuses_model->insert_status($title, $details);
                    $this->session->set_flashdata("status", "Status added successfully.");
                    redirect(site_url() . "statuses");
				}
			}
			
			$this->load->view("add_status_view", $data);
	//	}
	}


    public function search($search_keyword = "")
    {
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
        if (!$search_keyword) show_404();
        $search_keyword = htmlspecialchars_decode($search_keyword);
        $data = array();
        $current_page = (int) $this->uri->segment(3);
        $per_page = 1;
        $config["base_url"] = site_url() . "status_search/$search_keyword";
        $config['uri_segment'] = 3;
        $config["total_rows"] = $this->statuses_model->get_statuses_search_count($search_keyword);
        $config["per_page"] = $per_page;
        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
        $shipments = $this->statuses_model->get_all_by_keyword($search_keyword, $per_page, $current_page);


        if (isset($_POST['search']) AND !empty($_POST['search']))
        {
            redirect(site_url() . "status_search/".htmlspecialchars(trim($_POST['search'])));
        }

        if ($shipments)
        {
            $data["shipments"] = $shipments;
        }

        $this->load->view("manage_statuses_view", $data);
//		}
    }


	public function edit($id = "")
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();

			$data['status'] = $this->common_model->get_subject_with_token("statuses", "id", $id);
			if (!$id OR !$data['status']) show_404();

            if (isset($_POST["submit"]))
            {
                $title = htmlspecialchars(trim($_POST["title"]));
                $details = htmlspecialchars(trim($_POST["details"]));

                if (empty($title))
                {
                    $data["status"] = "<p class='error-msg'> Please fill in all required fields!</p>";
                }
                else
                {
                    $insert_id = $this->statuses_model->update_status($id, $title, $details);
                    $this->session->set_flashdata("status", "Status edited successfully.");
                    redirect(site_url() . "statuses");
                }
            }

			$this->load->view("edit_status_view", $data);
//		}
	}
	
	
	public function delete($id = "")
    {	

        $status = $this->common_model->get_subject_with_token("statuses", "id", $id);
        if (empty($id) OR ! $status) redirect(site_url() . "statuses");

        $this->common_model->delete_subject("statuses", "id", $id);

        $this->session->set_flashdata("status", "Deleted successfully! ");
        redirect($_SERVER['HTTP_REFERER']);

	}
	


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */