<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }

    public function get_active_customers()
    {
        $this->db->where('is_active', 1);
        $query = $this->db->get('customers');
        return $query->result_array();
    }

    public function get_active_drivers()
    {
        $this->db->where('is_active', 1);
        $this->db->where('type', 2);
        $this->db->where('customer_acc_id', $this->session->userdata('customer'));
        $query = $this->db->get('users');
        return $query->result_array();
    }


    public function get_shipments_monthly_report($customer_id, $status, $from_date, $to_date)
    {
        $this->db->order_by("shipment_code", 'desc');
        if (!empty($status))
        $this->db->where("status", $status);
        $this->db->where("cust_acc_id", $customer_id);
        $this->db->where("issue_datetime >=", $from_date);
        $this->db->where("issue_datetime <=", $to_date);
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $this->db->join('users', 'shipments.created_by = users.id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_driver_report($driver_id, $from_date, $to_date)
    {
        $this->db->order_by("shipment_movements.shipment_code", 'desc');
        $this->db->where("changed_by", $driver_id);
        $this->db->where("change_time >=", $from_date);
        $this->db->where("change_time <=", $to_date);
        $this->db->join('shipments', 'shipments.shipment_code = shipment_movements.shipment_code', 'left');
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $this->db->join('services', 'shipments.service_type = services.service_id', 'left');
        $this->db->join('users', 'shipments.created_by = users.id', 'left');
        $query = $this->db->get('shipment_movements');
        return $query->result_array();
    }

    public function get_courier_pickup_report($driver_id)
    {
        $date = date("Y-m-d");
        $f_date = date("Y-m-d 00-00-00", strtotime($date ." -15 day") );

        $this->db->where("changed_by", $driver_id);
        $this->db->where("shipments.status !=", 7);
        $this->db->where("shipment_movements.change_time >=", $f_date);
        $this->db->where("shipment_movements.to_status", 11);
        $this->db->join('shipment_movements', 'shipments.shipment_code = shipment_movements.shipment_code', 'left');
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_courier_ofd_report($driver_id, $customer_id)
    {
        $from_date = strtotime(date('Y-m-d H:i:s') . ' -10 days');
        $from_date = date('Y-m-d', $from_date);

        $this->db->where("shipments.cust_acc_id", $customer_id);
        $this->db->where("shipments.status", 8);
        $this->db->where("shipment_movements.changed_by", $driver_id);
        $this->db->where("shipment_movements.change_time >=", "shipments.received_at");
        $this->db->where("shipment_movements.change_time >=", "$from_date 23-59-59");
        $this->db->join('shipment_movements', 'shipments.shipment_code = shipment_movements.shipment_code', 'left');
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $this->db->join('customers', 'shipments.cust_acc_id = customers.customer_id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_courier_hal_report($driver_id)
    {
        $today=date("Y-m-d 00:00:00");
        $i = strtotime(date("Y-m-d", strtotime($today)) . " -4 days");
        $days_ago = date("Y-m-d 00:00:00" ,$i) ;


        $this->db->where("shipments.status", 16);
        $this->db->where("shipment_movements.changed_by", $driver_id);
        $this->db->where("shipment_movements.change_time >=", "shipments.received_at");
        $this->db->where("shipment_movements.change_time >=", $days_ago);
        $this->db->join('shipment_movements', 'shipments.shipment_code = shipment_movements.shipment_code', 'left');
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $this->db->join('customers', 'shipments.cust_acc_id = customers.customer_id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_ofd_report($from_date, $to_date)
    {
        $cities = array('jeddah', 'makkah', 'makakh');

        $city_condiation = array();
        foreach ($cities as $city)
        {
            $city_condiation [] = " (LOWER(cons_city)='$city' or LOWER(cons_city)  like '%$city%' or LOWER(cons_city)  like '$city%' or LOWER(cons_city)  like '%$city'";
            $city_condiation [] = "  LOWER(cons_address)='$city' or LOWER(cons_address)  like '%$city%' or LOWER(cons_address)  like '$city%' or LOWER(cons_address)  like '%$city')";
        }
        $city_condiation = implode('OR', $city_condiation);
        $this->db->where("shipments.received_at >=", $from_date);
        $this->db->where("shipments.received_at <=", $to_date);
        $this->db->where("shipments.status", 8);
        $this->db->where($city_condiation);
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $this->db->join('customers', 'shipments.cust_acc_id = customers.customer_id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_city_report($city, $from_date, $to_date)
    {
        $cities = array();
        switch (strtolower($city))
        {
            case "jeddah":
                $cities = array('jeddah', 'makkah', 'makakh');
                break;
            case "riyadh":
                $cities = array('riyadh');
                break;
            case "dammam":
                $cities = array('dammam','khobar','hasa','hofuf', 'jubail');
                break;
        }

        $city_condiation = array();
        foreach ($cities as $city)
        {
            $city_condiation [] = " (LOWER(cons_city)='$city' or LOWER(cons_city)  like '%$city%' or LOWER(cons_city)  like '$city%' or LOWER(cons_city)  like '%$city'";
            $city_condiation [] = "  LOWER(cons_address)='$city' or LOWER(cons_address)  like '%$city%' or LOWER(cons_address)  like '$city%' or LOWER(cons_address)  like '%$city')";
        }
        $city_condiation = implode('OR', $city_condiation);
        $this->db->select("shipments.shipment_code");
        $this->db->where("shipment_movements.change_time >=", $from_date);
        $this->db->where("shipment_movements.change_time <=", $to_date);
        $this->db->where("shipment_movements.to_status", 1);
        $this->db->where($city_condiation);
        $this->db->join('shipments', 'shipments.shipment_code = shipment_movements.shipment_code', 'left');
        $query = $this->db->get('shipment_movements');
        $shipments = $query->result_array();
        foreach ($shipments as &$shipment)
        {
            $shipment = $shipment['shipment_code'];
        }
        $shipments_codes = implode(',',$shipments);

        $this->db->where_in("shipment_movements.shipment_code", $shipments_codes);
        $this->db->where("shipment_movements.to_status !=", 1);
        $this->db->where("shipment_movements.to_status !=", "shipment_movements.from_status");
        $query = $this->db->get('shipment_movements');
        $shipments_ = $query->result_array();
        return array('shipments' => $shipments_, 'shipment_count' => count($shipments));
    }

    public function get_city_pickup_report($city, $from_date, $to_date)
    {
        $city_condiation = "";
        switch (strtolower($city))
        {
            case "jeddah":
                $city_condiation =" users.city_id IN (1,6) ";
                break;
            case "riyadh":
                $city_condiation =" users.city_id IN (2,328) ";
                break;
            case "dammam":
                $city_condiation =" users.city_id IN (8,4,23,3,556) ";
                break;
        }

        $this->db->select("shipments.shipment_code");
        $this->db->where("shipment_movements.change_time >=", $from_date);
        $this->db->where("shipment_movements.change_time <=", $to_date);
        $this->db->where("shipment_movements.to_status", 11);
        $this->db->where($city_condiation);
        $this->db->join('shipments', 'shipments.shipment_code = shipment_movements.shipment_code', 'left');
        $this->db->join('users', 'users.id = shipment_movements.changed_by', 'left');
        $query = $this->db->get('shipment_movements');
        $shipments = $query->result_array();
        foreach ($shipments as &$shipment)
        {
            $shipment = $shipment['shipment_code'];
        }
        $shipments_codes = implode(',',$shipments);

        $pickup = $this->shipments_counts_by_status_codes($shipments_codes, "(11,130,151,160,186)");
        $received = $this->shipments_counts_by_status_codes($shipments_codes, "(10,161)");
        $ofd = $this->shipments_counts_by_status_codes($shipments_codes, "(8,103,104,105,106,107,108,165)");
        $pod = $this->shipments_counts_by_status_codes($shipments_codes, "(7,146)");

        $this->db->where_in("shipment_movements.shipment_code", $shipments_codes);
        $this->db->where_not_in("shipments.status", "(1,7,146,10,161,11,130,151,160,186,8,103,104,105,106,107,108,165)");
        $this->db->where("shipment_movements.to_status !=", "shipment_movements.from_status");
        $this->db->join('shipments', 'shipments.shipment_code = shipment_movements.shipment_code', 'left');
        $query = $this->db->get('shipment_movements');
        $exception = $query->num_rows();

        return array('pickup'=>$pickup, 'received'=>$received, 'ofd'=>$ofd, 'pod'=>$pod,'exception'=>$exception);
    }

    public function shipments_counts_by_status_codes($shipments_codes, $to_statuses)
    {
        $this->db->where_in("shipment_movements.shipment_code", $shipments_codes);
        $this->db->where_in("shipment_movements.to_status", $to_statuses);
        $this->db->where("shipment_movements.to_status !=", "shipment_movements.from_status");
        $query = $this->db->get('shipment_movements');
        return $query->num_rows();
    }

    public function get_city_shipments_by_status($city, $status, $fdate, $tdate)
    {
        $cities = array();
        switch (strtolower($city))
        {
            case "jeddah":
                $cities = array('jeddah', 'makkah', 'makakh');
                break;
            case "riyadh":
                $cities = array('riyadh');
                break;
            case "dammam":
                $cities = array('dammam','khobar','hasa','hofuf', 'jubail');
                break;
        }

        $city_condiation = array();
        foreach ($cities as $city)
        {
            $city_condiation [] = " (LOWER(cons_city)='$city' or LOWER(cons_city)  like '%$city%' or LOWER(cons_city)  like '$city%' or LOWER(cons_city)  like '%$city'";
            $city_condiation [] = "  LOWER(cons_address)='$city' or LOWER(cons_address)  like '%$city%' or LOWER(cons_address)  like '$city%' or LOWER(cons_address)  like '%$city')";
        }
        $city_condiation = implode(' OR ', $city_condiation);

        $status_list="";
        if($status!=0)
        {

            if($status==1)
            {
                $status_list=" and to_status in ( 1)";
            }
            elseif ($status==11)
            {
                $status_list=" and to_status in ( 11,130,151,160,186)";
            }

            elseif ($status==10)
            {
                $status_list=" and to_status in ( 10,161)";
            }

            elseif ($status==8)
            {
                $status_list=" and to_status in ( 8,103,104,105,106,107,108,165)";
            }
            elseif ($status==7)
            {
                $status_list=" and to_status in ( 7,146)";
            }


        }
        else{

            $status_list=" and to_status NOT in ( 1,7,146,8,103,104,105,106,107,108,165	,10,161,11,130,151,160,186)";
        }
        $sql = "select * from  shipment_movements  , ( SELECT distinct(sh.shipment_code) as shipment_code ,cons_name , received_at , status FROM shipment_movements as hi ,shipments as sh where $city_condiation and change_time >='$fdate' and change_time <= '$tdate' and hi.shipment_code=sh.shipment_code	and to_status=1 ) as result1  where result1.shipment_code=shipment_movements.shipment_code  $status_list  ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_city_pickup_shipments_by_status($city, $status, $fdate, $tdate)
    {
        $cities = array();
        switch (strtolower($city))
        {
            case "jeddah":
                $cities = array('jeddah', 'makkah', 'makakh');
                break;
            case "riyadh":
                $cities = array('riyadh');
                break;
            case "dammam":
                $cities = array('dammam','khobar','hasa','hofuf', 'jubail');
                break;
        }

        $city_condiation = array();
        foreach ($cities as $city)
        {
            $city_condiation [] = " (LOWER(cons_city)='$city' or LOWER(cons_city)  like '%$city%' or LOWER(cons_city)  like '$city%' or LOWER(cons_city)  like '%$city'";
            $city_condiation [] = "  LOWER(cons_address)='$city' or LOWER(cons_address)  like '%$city%' or LOWER(cons_address)  like '$city%' or LOWER(cons_address)  like '%$city')";
        }
        $city_condiation = implode(' OR ', $city_condiation);

        $status_list="";
        if($status!=0)
        {

            if($status==1)
            {
                $status_list=" and to_status in ( 1)";
            }
            elseif ($status==11)
            {
                $status_list=" and to_status in ( 11,130,151,160,186)";
            }

            elseif ($status==10)
            {
                $status_list=" and to_status in ( 10,161)";
            }

            elseif ($status==8)
            {
                $status_list=" and to_status in ( 8,103,104,105,106,107,108,165)";
            }
            elseif ($status==7)
            {
                $status_list=" and to_status in ( 7,146)";
            }

            $sql = "select distinct(shipment_movements.shipment_code) , cons_name,status_notes , received_at , status  from  shipment_movements  , ( SELECT distinct(sh.shipment_code) as shipment_code ,cons_name,status_notes , received_at , status FROM shipment_movements as hi ,shipments as sh where $city_condiation and change_time >='$fdate' and change_time <= '$tdate' and hi.shipment_code=sh.shipment_code	and to_status=11 ) as result1  where result1.shipment_code=cl_shipment_history.shipment_code  $status_list  ";

        }
        else{

            $status_list=" and shipments.status NOT in ( 1,7,146,8,103,104,105,106,107,108,165,10,161,11,130,151,160,186)";
            $sql = "select * from  shipments  , ( SELECT distinct(sh.shipment_code) as shipment_code ,cons_name , received_at , status FROM shipment_movements as hi ,shipments as sh where $city_condiation and change_time >='$fdate' and change_time <= '$tdate' and hi.shipment_code=sh.shipment_code	and to_status=11 ) as result1  where result1.shipment_code=shipments.shipment_code  $status_list  ";

        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_driver_for_ofd_report($shipment_code)
    {
        $this->db->where("shipment_movements.to_status", 8);
        $this->db->where("shipment_movements.shipment_code", $shipment_code);
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $query = $this->db->get('shipment_movements');
        return $query->row_array();
    }

    public function get_shipment_movement_by_status($status, $shipment_code)
    {
        $this->db->where('to_status', 1);
        $this->db->where('shipment_code', $shipment_code);
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $query = $this->db->get('shipment_movements');
        return $query->result_array();
    }


    public function get_active_consignees_by_name($name, $customer_id)
    {
        $this->db->where('is_active', 1);
        $this->db->like('consignee_name', $name);
        if ($customer_id != 0) $this->db->where('cust_acc_id', $customer_id);
        $query = $this->db->get('consignees', 20);
        return $query->result_array();
    }

    public function get_active_consignees_by_code($code, $customer_id)
    {
        $this->db->where('is_active', 1);
        $this->db->like('consignee_id', $code);
        if ($customer_id != 0) $this->db->where('cust_acc_id', $customer_id);
        $query = $this->db->get('consignees', 20);
        return $query->result_array();
    }

	public function get_consignee_by_id($id)
	{
		$this->db->where('consignee_id', $id);
		$this->db->from('consignees');
        $this->db->join('cities', 'consignees.city_id = cities.city_id', 'left');
        $query = $this->db->get();
        return $query->result_array();
	}

    public function get_consignee_by_name($name)
	{
		$this->db->where('consignee_name', $name);
		$this->db->from('consignees');
        $this->db->join('cities', 'consignees.city_id = cities.city_id', 'left');
        $query = $this->db->get();
        return $query->result_array();
	}

    public function shipments_count()
    {
        //$this->db->where('consignee_id', $id);
        $this->db->order_by('shipment_code', 'desc');
        $query = $this->db->get('shipments');
        return $query->num_rows();
    }



    public function get_shipment_for_print($shipment_id)
    {
        $this->db->where("shipment_code", $shipment_id);
        $this->db->join('customers', 'shipments.cust_acc_id = customers.customer_id', 'left');
        $this->db->join('cities', 'customers.city_id = cities.city_id', 'left');
        $this->db->join('services', 'shipments.service_type = services.service_id', 'left');
        $query = $this->db->get('shipments');
        return $query->row_array();
    }

    public function get_shipment_by_id($id)
    {
        $this->db->where('shipment_code', $id);
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_bulk_shipments($bulk_codes, $limit, $offset)
    {
        $this->db->where("shipment_code IN ($bulk_codes)");
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $query = $this->db->get('shipments', $limit, $offset);
        return $query->result_array();
    }

    public function get_shipment_movements($id)
    {
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $this->db->where("shipment_movements.shipment_code", $id);
        $query = $this->db->get('shipment_movements');
        return $query->result_array();
    }

    public function insert_shipment($cons_name, $cons_code, $cust_id, $address, $city, $zip, $contact, $tel, $mob,
                                    $service_type, $package_type, $goods_desc, $payment, $is_dangerous, $notes, $number, $user_id)
    {
       $this->db->insert('shipments', array('cons_id' => $cons_code, 'cust_acc_id' => $cust_id, 'cons_name' => $cons_name ,'cons_address' => $address, 'cons_city' => $city, 'cons_zip' => $zip, 'cons_contact' => $contact,
			   'cons_telephone' => $tel, 'cons_mobile' => $mob, 'package_type' => $package_type, 'service_type' => $service_type, 'goods_description' =>$goods_desc, 'payment_method' =>$payment,
               'is_dangerous_good'=> $is_dangerous, 'notes' => $notes, 'status' => 1, 'items_count'=> $number, 'created_by' => $user_id));
       return $this->db->insert_id();
    }

    public function add_to_movement_history($id, $from_status, $to_status, $changed_by)
    {
        $this->db->insert('shipment_movements', array('shipment_code' => $id, 'from_status' => $from_status ,'to_status' => $to_status, 'change_time' => date("Y-m-d H:i:s"), 'changed_by' => $changed_by));

    }

    public function update_shipment_status($id, $status, $note, $received_date)
    {
        $this->db->where("shipment_code", $id);

        $this->db->update('shipments', array('status' => $status, 'status_notes' => $note, 'received_at' => $received_date));

    }

	public function edit_doctor($id, $name, $gender, $title, $section, $city, $area, $address, $phone, $work_time, $price, $describtion, $picture_name)
	{
		$this->db->where("id", $id);
		if (!empty($picture_name))
		{
			$this->db->update('doctors', array('name' => $name, 'gender' => $gender, 'title' => $title, 'section_id' => $section, 'city_id' => $city, 'area_id' => $area,
					'address' => $address, 'phone' => $phone, 'working_time' => $work_time, 'price' => $price, 'description' => $describtion, 'image' => $picture_name));
		}
		else
		{
			$this->db->update('doctors', array('name' => $name, 'gender' => $gender, 'title' => $title, 'section_id' => $section, 'city_id' => $city, 'area_id' => $area,
					'address' => $address, 'phone' => $phone, 'working_time' => $work_time, 'description' => $describtion, 'price' => $price));

		}

	}

    

	

}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */