<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" type="text/css" href="<?=ASSETS ?>css/shipment_styles.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Clex Form</title>
    <style type="text/css" media="print">
        .hide{display:none}
    </style>
    <style type="text/css">
        <!--
        .style7 {color: #FFFFFF; font-weight: bold; font-size: 14px; }
        -->
    </style>
</head>

<body bgcolor="#87c714">


<table border="0" cellspacing="1" cellpadding="0" align="center" class="maintable" bgcolor="#87c714" >

    <tr>
        <td colspan="4">
            <table width="800" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="500" class="tdcontent lm10">Courier/Agent: <?=@$shipments[0]['fullname']?></td>
                    <td width="100" class="tdcontent lm10">Shpts <?= count($shipments)?></td>
                </tr>

                <tr>
                    <td width="500" class="tdcontent lm10" colspan="2">Route: <?=@$shipments[0]['fullname']?></td>
                </tr>

                <tr>
                    <td width="500" class="tdcontent lm10" colspan="2">Print Date : <?= date("F d, Y h:i:sA");?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; By : <?=$this->session->userdata('name')?></td>
                </tr>
                <tr>
                    <td width="500" style="font-size: 25px; text-align: center; " class="tdcontent lm10" colspan="2"><?=@$shipments[0]['name']?></td>
                </tr>
            </table>
        </td>

    <tr>

<?php

    if(count($shipments) > 0):
        $counter = 1;
        foreach ($shipments as $shipment):

            $SIHPMENT_CODE = stripslashes($shipment['shipment_code']);
            $CUST_ACC = $shipment['name'];
            $From_Company = $shipment['company_name'];
            $To_Company = ($shipment['cons_name']);
            $To_TEL = stripslashes($shipment['cons_telephone']);
            $GOODS_DESC = stripslashes($shipment['goods_description']);
            $CURR_DATE = stripslashes($shipment['issue_datetime']);
            $CURR_USER = $shipment['created_by'];

        ?>
    <tr>
        <td colspan="4" valign="top">
            <table width="800" border="0" cellspacing="1" cellpadding="0" >
                <tr>
                    <td width="400" valign="top" class="tdcontent lm10">
                        <center>
                            <table border="0" cellspacing="2" cellpadding="2" width="100%">
                                <tr>
                                    <td valign="top"><font size=4><?=$counter?>
                                            <br />
                                        </font>
                                    </td>
                                    <td ><img src="<?= BARCODE ?>generateBarCode.php?code=<?=$SIHPMENT_CODE?>"  width="120" height="30"  />
                                    </td>
                                </tr>
                            </table>
                        </center>

                        <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2">
                            <tr>
                                <td width="20%">.........................................</td>

                                <td width="35%">
                                    <center>
                                        <font size=2><?=$SIHPMENT_CODE?> </font>
                                    </center>
                                </td>

                                <td width="50%">...............................</td>
                            </tr>
                            <tr>
                                <td colspan="2" width="50%"><font size=2>From : <?=$From_Company?></font></td>
                                <td width="50%"><font size=2> Tel : <?=$To_TEL?> </font></td>
                            </tr>
                            <tr>
                                <td colspan="2"><font size=2>  To : <?=$To_Company?> </font></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="3"><font size=2> Description of Goods : <?=$GOODS_DESC?></font></td>
                            </tr>

                        </table>


                    </td>

                    <td class="tdcontent lm10" width="100">
                        <font size=4>OFD<br />
                        </font><br />

                        <?=$CURR_DATE?> <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />


                    </td>
                    <td class="tdcontent lm10" width="100"></td>

                    <td class="tdcontent lm10" width="200">
                        <font size=4>Received by</font><br />

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />


                    </td>


                    <td class="tdcontent lm10" width="200">
                        <font size=4>Signature</font><br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />


                    </td>
                </tr>
            </table>
        </td>
    <tr>
        <?php
        $counter++;
       endforeach;
       endif;
        ?>

    <tr>

        <td colspan="2" valign="top" class="tm10">&nbsp;</td>
        <td colspan="2" valign="top" class="tm10">&nbsp;</td>
        <td width="1"><img src="images/spacer.gif" width="1" height="1" alt=""  /></td>
    </tr>

</table>


<!---------------------------- end  ----------------------------->
<script>
    window.print();
</script>

</body>
</html>
<?php exit; ?>