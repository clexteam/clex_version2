<?php
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=data.csv');
$output = fopen('php://output', 'w');
fputcsv($output, array('SERVICE TYPE', 'PACKAGE TYPE', 'SHIPMENT STATUS', 'POD DATE', 'ISSUE DATE', 'DATE', 'Received by', 'Client Name', 'SIHPMENT CODE' ));
foreach ($shipments as $shipment)
{
    switch ($shipment['package_type']) {
        case "M":
            $PACKAGE_TYPE="Medium Box";
            break;
        case "L":
            $PACKAGE_TYPE="Large Box";
            break;
        case "E":
            $PACKAGE_TYPE="Envelop";
            break;
        case "O":
            $PACKAGE_TYPE="Others";
            break;

        default:
            break;
    }



    fputcsv($output, array($shipment['service_name'], $PACKAGE_TYPE, $shipment['title'], $shipment['received_at'], $shipment['change_time'], $shipment['issue_datetime'], $shipment['fullname'], $shipment['cons_name'], $shipment['shipment_code']));

}

exit;