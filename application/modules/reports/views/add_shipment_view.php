<?php $this->load->view("header"); ?>

<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>

                <li>
                    <a href="#">Shipments</a>
                </li>
                <li class="active">Add Shipment</li>
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- /.nav-search -->
        </div>

        <div class="page-content">
            <div class="ace-settings-container" id="ace-settings-container">
                <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                    <i class="ace-icon fa fa-cog bigger-130"></i>
                </div>

                <div class="ace-settings-box clearfix" id="ace-settings-box">
                    <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <div class="pull-left">
                                <select id="skin-colorpicker" class="hide">
                                    <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                    <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                    <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                    <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                </select>
                            </div>
                            <span>&nbsp; Choose Skin</span>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                            <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                            <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                            <label class="lbl" for="ace-settings-add-container">
                                Inside
                                <b>.container</b>
                            </label>
                        </div>
                    </div><!-- /.pull-left -->

                    <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                            <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                            <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                            <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                        </div>
                    </div><!-- /.pull-left -->
                </div><!-- /.ace-settings-box -->
            </div><!-- /.ace-settings-container -->

            <div class="page-header">
                <h1>
                    Shipments
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                       Add shipment
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <form class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Name </label>

                            <div class="col-sm-9">
                                <input type="text" id="cons_name" name="cons_name" placeholder="Type consignee name.." class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Code </label>

                            <div class="col-sm-9">
                                <input type="text" id="cons_code" name="cons_code" placeholder="Type consignee code.." class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>


                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Address </label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-2" name="address" placeholder="Address" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> City </label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-2" name="city" placeholder="City" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-4">ZIP</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-2" name="zip" placeholder="ZIP" class="col-xs-10 col-sm-6" />

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-5">Contact</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-2" name="contact" placeholder="Contact" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Telephone</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-2" name="tel" placeholder="Tel" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-6">Mobile</label>
                            <div class="col-sm-9">
                                <input type="text" id="form-field-2" name="mob" placeholder="MOB" class="col-xs-10 col-sm-6"/>

                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">Service type</label>

                            <div class="col-sm-9">
                                <div class="inline">
                                    <select class="form-control" name="service_type" id="form-field-select-1" required>
                                        <?php foreach ($services as $service): ?>
                                            <option value="<?= $service['service_id'] ?>"><?= $service['service_name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">Package type</label>

                            <div class="col-sm-2">
                                <div class="inline">
                                    <select class="form-control" name="package_type" id="form-field-select-2">
                                        <option value="M">M Box</option>
                                        <option value="L">L Box</option>
                                        <option value="E" selected="">Envelop</option>
                                        <option value="O">Others</option>
                                    </select>
                                </div>
                            </div>
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-tags">Items Number</label>

                            <div class="col-sm-1">
                                <div class="inline">
                                    <input type="number" name="number" id="form-field-tags" value="1" placeholder="Items number" required/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">Goods Desc</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-2" name="goods_desc" placeholder="Goods desc" class="col-xs-10 col-sm-6" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-tags"> Payment</label>

                            <div class="col-sm-2">
                                <div class="inline">
                                    <select name="payment" class="form-control" id="form-field-select-3" required>
                                        <option value="A">Account</option>
                                        <option value="C">Cash</option>
                                        <option value="D">COD</option>
                                    </select>
                                </div>
                            </div>
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-tags"> Dangerous Goods</label>

                            <div class="col-sm-1">
                                <div class="inline">
                                    <select class="form-control" name="is_dangerous" id="form-field-select-5" required>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">  NOTES</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-2" name="notes" placeholder="Notes" class="col-xs-10 col-sm-6" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-tags"> Type</label>
                            <div class="radio">
                                <label>
                                    <input name="form-field-radio" type="radio" value="1" class="ace" required>
                                    <span class="lbl"> Corporate</span>
                                </label>
                                <label>
                                    <input name="form-field-radio" type="radio" value="0" class="ace" required>
                                    <span class="lbl"> Family</span>
                                </label>
                            </div>


                        </div>

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" name="submit" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>


                            </div>
                        </div>


                    </form>


                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<?php $this->load->view("footer"); ?>
<script src="<?=ASSETS?>js/jquery-ui.min.js"></script>

</body>
</html>
<script>

    $("#cons_name").autocomplete({
        minLength: 2,
        source: function( request, response ) {
            $.ajax({
                method: "POST",
                url: "<?= site_url()?>shipments/get_active_consignees_by_name",
                dataType: "json",
                data: { name: $("#cons_name").val() }
            }).done(function( data ) {
                response( $.map( data, function( item ) {
                    return {
                        label: item.consignee_name,
                        value: item.consignee_name
                    };
                }));
            });

        },

        select: function(event, ui) {
            $.post( "<?= site_url()?>shipments/get_consignee_data_byname", {name: ui.item.label},function( data ) {
                var obj = jQuery.parseJSON(data);
                $("[name='cons_code']").val(obj[0]['consignee_id']);
                $("[name='address']").val(obj[0]['address']);
                $("[name='city']").val(obj[0]['city']);
                $("[name='zip']").val(obj[0]['zip']);
                $("[name='contact']").val(obj[0]['contact']);
                $("[name='tel']").val(obj[0]['telephone']);
                $("[name='mob']").val(obj[0]['mobile']);
            });
        }
    })

    $("#cons_code").autocomplete({
        minLength: 1,
        source: function( request, response ) {
            $.ajax({
                method: "POST",
                url: "<?= site_url()?>shipments/get_active_consignees_by_code",
                dataType: "json",
                data: { code: $("#cons_code").val() }
            }).done(function( data ) {
                response( $.map( data, function( item ) {
                    return {
                        label: item.consignee_no,
                        value: item.consignee_id
                    };
                }));
            });

        },

        select: function(event, ui) {
            $.post( "<?= site_url()?>shipments/get_consignee_data_bycode", {code: ui.item.value},function( data ) {
                var obj = jQuery.parseJSON(data);
                $("[name='cons_name']").val(obj[0]['consignee_name']);
                $("[name='address']").val(obj[0]['address']);
                $("[name='city']").val(obj[0]['city']);
                $("[name='zip']").val(obj[0]['zip']);
                $("[name='contact']").val(obj[0]['contact']);
                $("[name='tel']").val(obj[0]['telephone']);
                $("[name='mob']").val(obj[0]['mobile']);

            });
        }
    })




if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true});
    //resize the chosen on window resize


    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
        if(event_name != 'sidebar_collapsed') return;
        $('.chosen-select').each(function() {
            var $this = $(this);
            $this.next().css({'width': $this.parent().width()});
        })
    });


}


</script>