<?php $this->load->view("header"); ?>


<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>

                <li>
                    <a href="#">Reports</a>
                </li>
                <li class="active">City Report</li>
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search" action="" method="post">
								<span class="input-icon">
									<input type="text" name="search" placeholder="Search shipments..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- /.nav-search -->
        </div>

        <div class="page-content">
            <div class="ace-settings-container" id="ace-settings-container">
                <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                    <i class="ace-icon fa fa-cog bigger-130"></i>
                </div>

                <div class="ace-settings-box clearfix" id="ace-settings-box">
                    <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <div class="pull-left">
                                <select id="skin-colorpicker" class="hide">
                                    <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                    <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                    <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                    <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                </select>
                            </div>
                            <span>&nbsp; Choose Skin</span>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                            <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                            <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                            <label class="lbl" for="ace-settings-add-container">
                                Inside
                                <b>.container</b>
                            </label>
                        </div>
                    </div><!-- /.pull-left -->

                    <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                            <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                            <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                            <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                        </div>
                    </div><!-- /.pull-left -->
                </div><!-- /.ace-settings-box -->
            </div><!-- /.ace-settings-container -->

            <div class="page-header">
                <h1>
                    Reports
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        City report
                    </small>
                </h1>
            </div><!-- /.page-header -->
            <?php if(!isset($shipments)): ?>
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <form class="form-horizontal" role="form" method="post">


                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">From date </label>

                            <div class="col-sm-9">
                                <input type="date" id="form-field-2" name="from_date" placeholder="" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">To date </label>

                            <div class="col-sm-9">
                                <input type="date" id="form-field-2" name="to_date" placeholder="" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>


                        <div class="space-4"></div>


                        <div class="form-group" id="customer_select">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> City  </label>

                            <div class="col-sm-4">
                                <select class="form-control" class="col-xs-6 col-sm-5" id="form-field-select-3"   name="city" >
                                    <option value=""> Select city </option>

                                        <option value="Riyadh" >Riyadh</option>
                                        <option value="Jeddah" >Jeddah</option>
                                        <option value="Dammam" >Dammam</option>

                                </select>
                            </div>
                        </div>

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" name="submit" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>


                            </div>
                        </div>


                    </form>


                </div><!-- /.col -->
            </div><!-- /.row -->
            <?php endif;  ?>
            <?php if(isset($shipments)): ?>
            <div class="row">
                <div class="col-xs-12">
                    <table id="simple-table" class="table  table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Status</th>
                            <th>Count</th>

                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        $Pickup = 0;
                        $Received = 0;
                        $OFD = 0;
                        $POD = 0;
                        $Printed = $printed_count;
                        $Exceptions = 0;
                        $Still_Printed = 0;
                        $SIHPMENT_CODE_list = "";
                        foreach ($shipments as $shipment) {
                            $SHIPMENT_STATUS = $shipment['to_status'];
                            if ($SHIPMENT_STATUS == 11 || $SHIPMENT_STATUS == 130 || $SHIPMENT_STATUS == 151 || $SHIPMENT_STATUS == 160 || $SHIPMENT_STATUS == 186)
                                $Pickup++;
                            elseif ($SHIPMENT_STATUS == 1)
                                $Still_Printed++; // do nothing
                            elseif ($SHIPMENT_STATUS == 10 || $SHIPMENT_STATUS == 161)
                                $Received++;
                            elseif ($SHIPMENT_STATUS == 8 || $SHIPMENT_STATUS == 103 || $SHIPMENT_STATUS == 104 || $SHIPMENT_STATUS == 105 || $SHIPMENT_STATUS == 106 || $SHIPMENT_STATUS == 107 || $SHIPMENT_STATUS == 108 || $SHIPMENT_STATUS == 165)
                                $OFD++;
                            elseif ($SHIPMENT_STATUS == 7 || $SHIPMENT_STATUS == 146)
                                $POD++;
                            else
                                $Exceptions++;
                        }

                        ?>

                        <tr>
                            <td><a href="<?=site_url() . 'reports/city_shipments?city=' . $city . '&status=1&fdate=' . $from_date . '&tdate=' .$to_date ?>">Printed</td>
                            <td><?= $printed_count?></td>
                        </tr>

                        <tr>
                            <td><a href="<?=site_url() . 'reports/city_shipments?city=' . $city . '&status=11&fdate=' . $from_date . '&tdate=' .$to_date ?>">Pickup Scan</td>
                            <td><?= $Pickup?></td>
                        </tr>

                        <tr>
                            <td><a href="<?=site_url() . 'reports/city_shipments?city=' . $city . '&status=10&fdate=' . $from_date . '&tdate=' .$to_date ?>">Received Station</td>
                            <td><?= $Received?></td>
                        </tr>

                        <tr>
                            <td><a href="<?=site_url() . 'reports/city_shipments?city=' . $city . '&status=8&fdate=' . $from_date . '&tdate=' .$to_date ?>">OFD</td>
                            <td><?= $OFD?></td>
                        </tr>

                        <tr>
                            <td><a href="<?=site_url() . 'reports/city_shipments?city=' . $city . '&status=7&fdate=' . $from_date . '&tdate=' .$to_date ?>">POD</td>
                            <td><?= $POD?></td>
                        </tr>

                        <tr>
                            <td><a href="<?=site_url() . 'reports/city_shipments?city=' . $city . '&status=0&fdate=' . $from_date . '&tdate=' .$to_date ?>">Exceptions</td>
                            <td><?= $Exceptions?></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div><!-- /.row -->
      <?php endif; ?>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->


<?php $this->load->view("footer"); ?>
<script>

    if(!ace.vars['touch']) {
        $('.chosen-select').chosen({allow_single_deselect:true});
        //resize the chosen on window resize


        //resize chosen on sidebar collapse/expand
        $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
            if(event_name != 'sidebar_collapsed') return;
            $('.chosen-select').each(function() {
                var $this = $(this);
                $this.next().css({'width': $this.parent().width()});
            })
        });


    }
</script>
</body>
</html>
