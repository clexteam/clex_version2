<?php
/**
 * Created by PhpStorm.
 * User: remon
 * Date: 4/4/17
 * Time: 8:28 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model("home_model");
    }

    public function index()
    {
        $this->deny->deny_if_logged_out();
        redirect(site_url() . 'shipments/add');
        //$this->load->view('home_view');
    }

    public function move_cons()
    {
        $consignees = $this->home_model->select_cons(50000, 0);
        $this->home_model->truncate_table('consignees');
        foreach ($consignees as $consignee)
        {echo $consignee['CONS_NO']."<br>";
            if($consignee['customer_id'] != null)
            {
                if ($consignee['city_id'] == null) $city = 0; else $city = $consignee['city_id'];
                if ($consignee['IS_ACTIVE'] == 'Y') $active = 1; else $active = 0;
                if ($consignee['customer_id'] == null) $customer_id = 0; else $customer_id = $consignee['customer_id'];
                $this->home_model->insert_consignee($consignee['CONS_NO'], $customer_id, 0, $consignee['CONS_NAME'], $consignee['CONS_DISTRICT'],
                    $consignee['CONS_STREET'], $consignee['CONS_ADDRESS'], $city, $consignee['CONS_ZIP'], $consignee['CONS_CONTACT'],
                    $consignee['CONS_TEL'], $consignee['CONS_MOB'], $active, $consignee['REMARKS']);
            }

        }
        //print_r($consignees);exit;
    }

    public function move_customers()
    {
        $customers = $this->home_model->select_customers();
        foreach ($customers as $customer)
        {
            if ($customer['city_id'] == null) $city = 0; else $city = $customer['city_id'];
            $this->home_model->insert_customer($customer['CUST_ACC'], $customer['SH_COMPANY'], $customer['SH_DISTRICT'], $customer['STREET'],
                $city, $customer['SH_ZIP'], $customer['SH_CONTACT'], $customer['SH_TEL'], $customer['SH_MOB'], 1);
        }
        print_r($customers);exit;
    }

    public function move_users()
    {
        $users = $this->home_model->select_users();
        foreach ($users as $user)
        {
            $password = password_hash($user['USER_PWD'], PASSWORD_DEFAULT);
            if ($user['IS_ACTIVE'] == 'Y') $active = 1; else $active = 0;
            if ($user['city_id'] == null) $city = 0; else $city = $user['city_id'];
            $this->home_model->insert_user($user['USER_ID'], $user['customer_id'], $user['USER_NAME'], $user['USER_NAME'], $password,
                    1, $city, $active, $user['MOBILE']);
        }
        print_r($users);exit;
    }

    public function move_shipments()
    {
        $this->home_model->delete_last_month_shipments();
        $max_shipment_code_in_live =  $this->home_model->get_max_id('livedb', 'SIHPMENT_CODE', 'CL_SHIPMENT');
        $max_shipment_code_in_local =  $this->home_model->get_max_id('default', 'shipment_code', 'shipments');
        echo $max_shipment_code_in_live['max_code']. "<br>" . $max_shipment_code_in_local['max_code'];
        if ($max_shipment_code_in_live['max_code'] > $max_shipment_code_in_local['max_code'])
        {
            $shipments = $this->home_model->select_shipments($max_shipment_code_in_local['max_code']);
            foreach ($shipments as $shipment)
            {
                $service_type = "";
                if ($shipment['SERVICE_TYPE'] == 'D') $service_type = 1;
                elseif ($shipment['SERVICE_TYPE'] == 'C') $service_type = 2;
                elseif ($shipment['SERVICE_TYPE'] == 'X') $service_type = 3;
                elseif ($shipment['SERVICE_TYPE'] == 'S') $service_type = 4;
                elseif ($shipment['SERVICE_TYPE'] == 'I') $service_type = 5;
                else $service_type = 6;
                if ($shipment['DANGEROUS_GOODS'] == "N") $dangerous_good = 0; else  $dangerous_good = 1;
                if ($shipment['IS_CHARGEABLE'] == "N") $chargeable = 0; else  $chargeable = 1;
                $this->home_model->insert_shipment($shipment['SIHPMENT_CODE'], $shipment['cons_type'], $shipment['third_party_code'], $shipment['CONS_NAME'], $shipment['CONS_NO'], $shipment['customer_id'],
                    $shipment['CONS_DISTRICT'], $shipment['CONS_STREET'], $shipment['CONS_ADDRESS'], $shipment['CONS_CITY'], $shipment['CONS_ZIP'],
                    $shipment['CONS_CONTACT'], $shipment['CONS_TEL'], $shipment['CONS_MOB'], $service_type, $shipment['PACKAGE_TYPE'], $shipment['GOODS_DESC'],
                    $shipment['PAYMENT_METHOD'], $dangerous_good, $shipment['NOTES'], $shipment['SHIPMENT_STATUS'], $shipment['STATUS_NOTES'], $shipment['IP_ADDRESS'],
                    $shipment['RECEIVED_DATE'], $chargeable, $shipment['RATE'], $shipment['CLEX_NOTE'], $shipment['latitude'], $shipment['longitude'], $shipment['signature'],
                    $shipment['ITEMS_NUMBER'], $shipment['CURR_USER'], $shipment['CURR_DATE']);
            }
        }


    }

    public function move_shipment_movements()
    {
        $max_id_in_live =  $this->home_model->get_max_id('livedb', 'ID', 'cl_shipment_history');
        $max_id_in_local =  $this->home_model->get_max_id('default', 'id', 'shipment_movements');
        echo $max_id_in_live['max_code']. "<br>" . $max_id_in_local['max_code'];
        if ($max_id_in_live['max_code'] > $max_id_in_local['max_code'])
        {
            $shipments = $this->home_model->select_shipment_movements($max_id_in_local['max_code']);
            foreach ($shipments as $shipment)
            {
                $this->home_model->add_to_movement_history($shipment['ID'], $shipment['SIHPMENT_CODE'], $shipment['FROM_STATUS'], $shipment['TO_STATUS'], $shipment['USER_ID'], $shipment['CHANGE_TIME']);
            }
        }

    }

    public function test()
    {
        $this->home_model->truncate_table();

    }

    public function login()
    {
        $this->deny->deny_if_logged_in();

        $data = array();

        if (isset($_POST["submit"]))
        {
            $username = trim($_POST["username"]);
            $password = trim($_POST["password"]);

            if (empty($username) OR empty($password))
            {
                $data["error"] = "Please enter your username and password!";
            }
            else
            {
                if ($user_info = $this->home_model->login($username, $password))
                {
                    extract($user_info);

                    $session_data = array(
                        "logged_in" => TRUE,
                        "id" => $id,
                        "customer" => $customer_acc_id,
                        "name" => $fullname,
                        "type" => $type,
                        "username" => $username,
                    );

                    $this->session->set_userdata($session_data);

                    $_SESSION["logged_in"] = TRUE;

                    // Redirect user to admin home page
                    redirect(site_url());


                }
                else
                {
                    $data["error"] = "Username or/and password is/are invalid!";

                }
            }
        }

        $this->load->view("login_view", $data);
    }

    public function logout()
    {
        $this->deny->deny_if_logged_out();

        $this->session->sess_destroy();
        session_destroy();

        redirect(site_url() . "login");
    }
}