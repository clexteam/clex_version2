<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function login($username, $password)
    {
        $sql = "SELECT * FROM `users` WHERE `username` = ? AND is_active = 1";
        $query = $this->db->query($sql, array($username));

        if ($query->num_rows() >= 1)
        {
            $row = $query->row_array();
            $stored_hash = $row["password"];

            if (password_verify($password, $stored_hash))
            {
                return $row;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }


    public function update_user_password($new_password, $user_id)
    {
        $new_password = password_hash($new_password, PASSWORD_DEFAULT);

        $sql = "UPDATE `users_details` SET `password` = ? WHERE `id` = ?";
        $query = $this->db->query($sql, array($new_password, $user_id));
    }


    public function update_user_picture($new_picture, $user_id)
    {
        $sql = "UPDATE `users_details` SET `picture` = ? WHERE `id` = ?";
        $query = $this->db->query($sql, array($new_picture, $user_id));
    }

    public function select_cons($limit, $offset)
    {
        $live_db = $this->load->database('livedb', TRUE);
        $live_db->join('cities', 'CL_CONS.CONS_CITY = cities.city', 'left');
        $live_db->join('customers', 'CL_CONS.CUST_ACC = customers.name', 'left');
        $query = $live_db->get('CL_CONS', $limit, $offset);
        return $query->result_array();
    }

    public function select_customers()
    {
        $this->db->join('cities', 'cl_customer.SH_CITY = cities.city', 'left');
        $query = $this->db->get('cl_customer');
        return $query->result_array();
    }

    public function select_users()
    {
        $this->db->join('cities', 'SEC_USER.city = cities.city', 'left');
        $this->db->join('customers', 'SEC_USER.CUST_ACC = customers.name', 'left');
        $query = $this->db->get('SEC_USER');
        return $query->result_array();
    }

    public function get_max_id($db, $attribute, $table)
    {
        $db = $this->load->database($db, TRUE);
        $query = $db->query("SELECT MAX($attribute) as max_code FROM $table");
        return $query->row_array();
    }

    public function delete_last_month_shipments()
    {
        $this->db->where("issue_datetime > NOW() - INTERVAL 30 DAY");
        $query = $this->db->delete('shipments');
    }

    public function select_shipments($max_id)
    {
        $live_db = $this->load->database('livedb', TRUE);
        if (!empty($max_id)) $live_db->where("SIHPMENT_CODE >", $max_id);
        $live_db->where("CURR_DATE >=", "2017-01-01");
        //$live_db->join('consignees', 'CL_SHIPMENT.CONS_NAME = consignees.consignee_name', 'left');
        $live_db->join('customers', 'CL_SHIPMENT.CUST_ACC = customers.name', 'left');
        $query = $live_db->get('CL_SHIPMENT', 100000);
        return $query->result_array();
    }

    public function select_shipment_movements($max_id)
    {
        $live_db = $this->load->database('livedb', TRUE);
        if (!empty($max_id)) $live_db->where("ID >", $max_id);
        $live_db->where("CHANGE_TIME >=", "2017-01-01");
        $query = $live_db->get('cl_shipment_history');
        return $query->result_array();
    }

    public function insert_shipment($shipment_code, $type, $partner_code, $cons_name, $cons_code, $cust_id, $district, $street, $address, $city, $zip, $contact, $tel, $mob,
                                    $service_type, $package_type, $goods_desc, $payment, $is_dangerous, $notes, $status, $status_notes, $ip, $received_at, $is_chargeable, $rate,
                                    $clex_notes, $cons_lat, $cons_long, $signature, $number, $user_id, $issue_datetime)
    {
        $this->db->insert('shipments', array('shipment_code' => $shipment_code, 'type' => $type, 'partner_code' => $partner_code,
            'cons_id' => $cons_code, 'cust_acc_id' => $cust_id, 'cons_name' => $cons_name , 'cons_district' => $district, 'cons_street' => $street, 'cons_address' => $address, 'cons_city' => $city, 'cons_zip' => $zip, 'cons_contact' => $contact,
            'cons_telephone' => $tel, 'cons_mobile' => $mob, 'package_type' => $package_type, 'service_type' => $service_type, 'goods_description' =>$goods_desc, 'payment_method' =>$payment,
            'is_dangerous_good'=> $is_dangerous, 'notes' => $notes, 'status' => $status, 'status_notes' => $status_notes, 'ip_address' => $ip, 'received_at' => $received_at,
            'is_chargeable' => $is_chargeable, 'rate' => $rate, 'clex_note' => $clex_notes, 'cons_latitude' =>$cons_lat, 'cons_longitude' => $cons_long,
            'signature' => $signature, 'items_count'=> $number, 'created_by' => $user_id, 'issue_datetime' => $issue_datetime));
        return $this->db->insert_id();
    }

    public function add_to_movement_history($id, $shipment_id, $from_status, $to_status, $changed_by, $change_time)
    {
        $this->db->insert('shipment_movements', array('id'=> $id, 'shipment_code' => $shipment_id, 'from_status' => $from_status ,'to_status' => $to_status, 'change_time' => $change_time, 'changed_by' => $changed_by));

    }

    public function truncate_table($table)
    {
        $this->db->query("truncate table $table");
    }

    public function insert_customer($name, $company, $district, $street, $city, $zip, $contact, $tel, $mob, $active)
    {
        $this->db->insert('customers', array('name' => $name, 'company_name' => $company, 'district' => $district ,'street' => $street, 'city_id' => $city, 'zip' => $zip, 'contact' => $contact,
            'telephone' => $tel, 'mobile' => $mob, 'is_active' => $active));
        return $this->db->insert_id();
    }

    public function insert_user($user_id, $customer, $user_name, $username, $password, $type, $city, $active, $mobile)
    {
        $this->db->insert('users', array('id' => $user_id, 'customer_acc_id' => $customer, 'fullname' => $user_name, 'username' => $username ,'password' => $password, 'type' => $type, 'city_id' => $city, 'is_active' => $active,
            'user_mobile' => $mobile));
        return $this->db->insert_id();
    }


    public function insert_consignee($consignee_id, $customer_id, $type, $name, $district, $street, $address, $city, $zip, $contact,
            $tel, $mob, $active, $remarks)
    {
        $this->db->insert('consignees', array('consignee_no' => $consignee_id,'cust_acc_id' => $customer_id, 'type' => $type, 'consignee_name' => $name ,'district' => $district,
            'street' => $street, 'address' => $address, 'city_id' => $city, 'zip' => $zip, 'contact' => $contact,
            'telephone' => $tel, 'mobile' => $mob, 'is_active' => $active, 'remarks' => $remarks));
        return $this->db->insert_id();
    }

}


/* End of file login_model.php */
/* Location: ./application/modules/home/models/login_model.php */