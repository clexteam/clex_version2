<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipments extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("shipments_model");
    }
	
	
	public function index()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
			$current_page = (int) $this->uri->segment(2);
			$per_page = 10;
			$shipments_count = $this->shipments_model->shipments_count();
			$config["base_url"] = site_url() . "shipments/";
			$config['uri_segment'] = 2;
			$config["total_rows"] = $shipments_count;
			$config["per_page"] = $per_page;

            //config for bootstrap pagination class integration
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			$shipments = $this->shipments_model->get_all($per_page, $current_page);
			if (isset($_POST['search']) AND !empty($_POST['search']))
			{
                redirect(site_url() . "search/".htmlspecialchars(trim($_POST['search'])));
            }

			if ($shipments)
			{
				$data["shipments"] = $shipments;
			}
			
			$this->load->view("manage_shipments_view", $data);
//		}
	}

    /**
     * @return ConsigneesArray
     */
    public function get_active_consignees_by_name()
    {
        $name = htmlspecialchars($_POST['name']);
        $consignees = $this->shipments_model->get_active_consignees_by_name($name, $this->session->userdata("customer"));
        echo json_encode($consignees);
    }

    /**
     * @return ConsigneesArray
     */
    public function get_active_consignees_by_code()
    {
        $code = htmlspecialchars($_POST['code']);
        $consignees = $this->shipments_model->get_active_consignees_by_code($code, $this->session->userdata("customer"));
        echo json_encode($consignees);
    }
	
	public function add()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
			$data['consignees'] = $this->shipments_model->get_active_consignees_by_name('test',$this->session->userdata("customer"));
            $data['services'] = $this->common_model->get_all_from_table("services");

			if (isset($_POST["submit"]))
			{
                $cons_arr = explode('_', $_POST["cons_name"]);
                $cons_name = htmlspecialchars(trim($_POST["cons_name"]));
				$cons_code = htmlspecialchars(trim($_POST["cons_code"]));
				$address = htmlspecialchars(trim($_POST["address"]));
                $city = htmlspecialchars(trim($_POST["city"]));
                $zip = @htmlspecialchars(trim($_POST["zip"]));
                $contact = @htmlspecialchars(trim($_POST["contact"]));
                $tel = @htmlspecialchars(trim($_POST["tel"]));
                $mob = @htmlspecialchars(trim($_POST["mob"]));
				$service_type = $_POST['service_type'];
				$package_type = $_POST['package_type'];
                $goods_desc = @htmlspecialchars(trim($_POST["goods_desc"]));
                $payment = $_POST['payment'];
                $number = $_POST['number'];
                $is_dangerous = $_POST['is_dangerous'];
				$notes = @htmlspecialchars(trim($_POST["notes"]));

				if (empty($cons_name) OR empty($cons_code) OR empty($address) OR empty($city) OR empty($contact) OR empty($tel))
				{
					$data["status"] = "<p class='error-msg'> Please fill in all required fields!</p>";
				}
				else
				{
                    $insert_id = $this->shipments_model->insert_shipment($cons_name, $cons_code, $this->session->userdata("customer"),  $address,$city, $zip, $contact,
                                 $tel, $mob, $service_type, $package_type, $goods_desc, $payment, $is_dangerous, $notes, $number, $this->session->userdata("id"));
                    $this->session->set_flashdata("status", "Shipment added successfully.");
                    redirect(site_url() . "shipments/print_shipment/".$insert_id);
				}
			}
			
			$this->load->view("add_shipment_view", $data);
	//	}
	}

    public function get_consignee_data_byname()
    {
        $name = $_POST['name'];
        $consignee = $this->shipments_model->get_consignee_by_name($name);
        echo json_encode($consignee);
    }

    public function get_consignee_data_bycode()
    {
        $code = $_POST['code'];
        $consignee = $this->shipments_model->get_consignee_by_id($code);
        echo json_encode($consignee);
    }

    public function print_shipment($shipment_id)
    {
        $shipment = $this->common_model->get_subject_with_token("shipments", "shipment_code", $shipment_id);
        if (!$shipment_id OR !$shipment) show_404();

        $data['code'] = $shipment_id;
        $data['shipment'] = $this->shipments_model->get_shipment_for_print($shipment_id);
        $this->load->view("print_shipment_view", $data);
    }

    public function edit_status($id)
    {
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
        $shipment = $this->common_model->get_subject_with_token("shipments", "shipment_code", $id);
        if (!$id OR !$shipment) show_404();
        $data = array();
        $data['shipment'] = $shipment;
        $data['shipment']['service_type'] = $this->common_model->get_subject_with_token("services", "service_id", $shipment['service_type']);
        $data['statuses'] = $this->common_model->get_all_from_table("statuses");

        if (isset($_POST["submit"]))
        {
            $status = $_POST["status"];
            $note = @htmlspecialchars(trim($_POST["status_notes"]));
            $received_date = htmlspecialchars(trim($_POST["received_date"]));

            if (empty($status) OR empty($note) OR empty($received_date))
            {
                $data["status"] = "<p class='error-msg'> Please fill in all required fields!</p>";
            }
            else
            {
                $this->shipments_model->add_to_movement_history($id, $shipment['status'], $status, $this->session->userdata("id"));
                $this->shipments_model->update_shipment_status($id, $status, $note, $received_date);
                $this->session->set_flashdata("status", "Shipment updated successfully.");
                redirect(site_url() . "shipments");
            }
        }

        $this->load->view("edit_shipment_status_view", $data);
        //	}
    }

    public function search($search_keyword = "")
    {
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
        if (!$search_keyword) show_404();
        $search_keyword = htmlspecialchars_decode($search_keyword);
        $data = array();
        if (is_numeric($search_keyword))
        {
            $shipments = $this->shipments_model->get_shipment_by_id($search_keyword);
        }
        else
        {
            $shipments_codes_array = explode('-', $search_keyword);
            $shipments_codes = str_replace('-',',',$search_keyword);
            $current_page = (int) $this->uri->segment(3);
            $per_page = 1;
            $config["base_url"] = site_url() . "search/$search_keyword";
            $config['uri_segment'] = 3;
            $config["total_rows"] = count($shipments_codes_array);
            $config["per_page"] = $per_page;
            //config for bootstrap pagination class integration
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            $shipments = $this->shipments_model->get_bulk_shipments($shipments_codes, $per_page, $current_page);
        }

        if (isset($_POST['search']) AND !empty($_POST['search']))
        {
            redirect(site_url() . "search/".htmlspecialchars(trim($_POST['search'])));
        }

        if ($shipments)
        {
            $data["shipments"] = $shipments;
        }

        $this->load->view("search_shipments_view", $data);
//		}
    }

    public function movement($id)
    {
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
        $shipment = $this->common_model->get_subject_with_token("shipments", "shipment_code", $id);
        if (!$id OR !$shipment) show_404();
        $data = array();
        $data['shipment'] = $shipment;
        $movements = $this->shipments_model->get_shipment_movements($id);
        foreach ($movements as &$movement)
        {
            $movement['from_status_data'] = $this->common_model->get_subject_with_token("statuses", "id", $movement['from_status']);
            $movement['to_status_data'] = $this->common_model->get_subject_with_token("statuses", "id", $movement['to_status']);
        }

        $data['movements'] = $movements;
        $this->load->view("shipment_movement_view", $data);
        //	}
    }

    public function areas_of_city()
	{
		$city_id = $_POST['city'];
		$areas = $this->doctors_model->get_areas_of_city($city_id);
		echo json_encode($areas);
	}

	
	public function view($id = "")
	{	
		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
		if ($authorized)
		{
			$section = $this->common_model->get_subject_with_token("sections", "id", $id);
			if (empty($id) OR ! $section) show_404();
			
			$data["section"] = $section;
			
			echo $this->load->view("ajax_view_section_view", $data, TRUE);
		}
	}
	
	
	public function edit($id = "")
	{
		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
		if ($authorized)
		{
			$data = array();
			$data['cities'] = $this->common_model->get_all_from_table("cities");
			$data['sections'] = $this->common_model->get_all_from_table("sections");
			$data['doctor'] = $this->common_model->get_subject_with_token("doctors", "id", $id);
			$data['areas'] = $this->doctors_model->get_areas_of_city($data['doctor']['city_id']);

			if (isset($_POST["submit"]))
			{
				$name = htmlspecialchars(trim($_POST["name"]));
				$gender = $_POST['gender'];
				$title = htmlspecialchars(trim($_POST["title"]));
				$section = $_POST['section'];
				$city = $_POST['city'];
				$area = $_POST['area'];
				$address = @htmlspecialchars(trim($_POST["address"]));
				$phone = @htmlspecialchars(trim($_POST["phone"]));
				$work_time = @htmlspecialchars(trim($_POST["work_time"]));
				$price = @htmlspecialchars(trim($_POST["price"]));
				$describtion = @htmlspecialchars(trim($_POST["describtion"]));
				$image_name = @$_FILES["image"]["name"];

				if (empty($name) OR empty($title))
				{
					$data["status"] = "<p class='error-msg'> يجب إدخال جميع البيانات الإجبارية</p>";
				}
				else
				{
					if (!empty($image_name))
					{
						$tmp_picture_name = $_FILES["image"]["tmp_name"];
						$allowed_exts = array("jpg");
						$a_long = explode(".", $image_name);
						$file_ext = strtolower(end($a_long));
						unset($a_long);
						$path = DOCTORS_PHOTOS_PATH;

						// Restricting file uploading to zip files only
						if (!in_array($file_ext, $allowed_exts))
						{
							$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
						}
						else
						{
							// Success. Give the pictures a unique (same) name and upload them, then insert data and log action
							if (! empty($data['doctor']['image'])) unlink($path . $data['doctor']['image']);
							$cur_time = time();
							$picture_name = $cur_time . "." . $file_ext;
							move_uploaded_file($tmp_picture_name, $path . $picture_name);
							$this->doctors_model->edit_doctor($id, $name, $gender, $title, $section, $city, $area, $address, $phone, $work_time, $price, $describtion, $picture_name);
							$this->session->set_flashdata("status", "تمت العملية بنجاح");
							redirect(site_url() . "doctors");
						}
					}
					else
					{
						//-- if user doesn't select the image
						$this->doctors_model->edit_doctor($id, $name, $gender, $title, $section, $city, $area, $address, $phone, $work_time, $price, $describtion, '');
						$this->session->set_flashdata("status", "تمت العملية بنجاح");
						redirect(site_url() . "doctors");
					}
				}
			}

			$this->load->view("edit_doctor_view", $data);
		}
	}
	
	
	public function delete($id = "")
    {	
		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
		if ($authorized)
		{
			$doctor = $this->common_model->get_subject_with_token("doctors", "id", $id);
			if (empty($id) OR ! $doctor) redirect(site_url() . "doctors");
		
			$this->common_model->delete_subject("doctors", "id", $id);
			
			$this->session->set_flashdata("status", "تمت العملية بنجاح");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */