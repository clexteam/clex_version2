<?php
function ConvertToOrcaleFormate($CURR_DATE)
{

    $DateTimeFormate=explode(":",$CURR_DATE);
    $DateFormate=explode("-",$DateTimeFormate[0]);
    $TimeFormate=explode("-",$DateTimeFormate[1]);

    return  @date("l F d Y H:i",mktime($TimeFormate[0],$TimeFormate[1],$TimeFormate[2],$DateFormate[1],$DateFormate[2],$DateFormate[0]));
}

$cons_city = mb_strtolower($shipment['cons_city']);
$find_address="";
if(($cons_city == "jeddah") || ($cons_city == "makkah") || ($cons_city == "makakh")){
    $City_Char="J";
    $find_address=true;
}


if($find_address==false){
    if($cons_city =="riyadh"){
        $City_Char="R";
        $find_address=true;
    }

}


if($find_address==false){
    if(($cons_city == "dammam") || ($cons_city == "khobar") || ($cons_city == "hasa") || ($cons_city == "hofuf")  || ($cons_city == "jubail") ){
        $City_Char="D";
        $find_address=true;
    }

}

$payment_method="";
switch ($shipment['payment_method']) {
    case "A":
        $payment_method="Account";
        break;
    case "C":
        $payment_method="Cash";
        break;
    case "D":
        $payment_method="COD";
        break;
}

$is_dangerous_goods="";
switch ($shipment['is_dangerous_good']) {
    case "1":
        $is_dangerous_goods="Yes";
        break;
    case "0":
        $is_dangerous_goods="No";
        break;

}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" type="text/css" href="<?= ASSETS ?>css/shipment_styles.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Clex Form</title>
    <style type="text/css" media="print">
        .hide{display:none}
    </style>
    <style type="text/css">
        <!--
        .style7 {color: #FFFFFF; font-weight: bold; font-size: 14px; }
        -->

    </style>
</head>

<body bgcolor="#87c714">
<?php for ($i=0 ; $i< 2; $i++): ?>
<center><a href='<?=site_url() ?>shipments/add' class="hide">New Shipment</a></center>


<table border="0" cellspacing="2" cellpadding="0" align="center" class="maintable" bgcolor="#87c714">

    <tr>

        <td colspan="2" valign="top" class="tm10">
            <table width="350" border="0" cellspacing="3" cellpadding="0">


                <tr>
                    <td class="tdcontent lm10" valign="bottom"><table cellpadding="0" cellspacing="0"><tr><td class="tdcontent">1.Account No. </td><td>
                                  <?php $word = str_split($shipment['name']) ?>
                                    <?php foreach ($word as $char): ?>
                                    <input type="text" class="INPUT1"  value="<?=$char?>" />&nbsp;&nbsp;
                                    <?php endforeach; ?>

                                </td></tr></table></td>

                </tr>

                <tr>
                    <td class="tdcontent tm5 bm3 rm5 lm5">
                        <table width="100%" cellpadding="0" cellspacing="0" ><tr><td colspan="2" class="tdtable2"> 2.Form(SHIPPER)</td></tr>
                            <tr><td colspan="2" class="tdtable2">Company: <font color="Red"><?= $shipment['company_name'] ?></font></td></tr>
                            <tr><td class="tdtable2" colspan="2">Address: <font color="Red"><?= $shipment['street'] . ', '. $shipment['district'] ?></font></td></tr>
                            <tr>
                                <td  class="tdtable2" width="40%">City: <font color="Red"><?= $shipment['city'] ?></font></td><td class="tdtable2">Zip Code:<font color="Red"><?= $shipment['zip'] ?></font></td></tr>
                            <tr>
                                <td  class="tdtable2" colspan="2">
                                    Contact Name:<font color="Red"><?= $shipment['contact'] ?></font>
                                </td>
                            </tr>
                            <tr><td class="tdtable2">Telephone:<font color="Red"><?= $shipment['telephone'] ?></font></td><td  class="tdtable2">Mobile:<font color="Red"><?= $shipment['mobile'] ?></font></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td class="tdcontent tm5 bm3 rm5 lm5">


                        <table width="100%" cellpadding="0" cellspacing="0"><tr><td colspan="2" class="tdcontent">4.To(CONSIGNEE)</td></tr>
                            <tr><td colspan="2" class="tdtable2">Company: <font color="Red"><?= $shipment['cons_name'] ?></font></td></tr>
                            <tr><td class="tdtable2" colspan="2">Address: <font color="Red"><?= $shipment['cons_address'] ?></font></td></tr>
                            <tr>
                                <td  class="tdtable2" width="40%">City: <font color="Red"><?= $shipment['cons_city'] ?></font></td><td class="tdtable2">Zip Code: <font color="Red"><?= $shipment['cons_zip'] ?></font></td></tr>

                            <tr>
                                <td  class="tdtable2" colspan="2">
                                    Contact Name:<font color="Red"><?= $shipment['cons_contact'] ?></font>
                                </td>
                            </tr>
                            <tr><td class="tdtable2" colspan="2">Telephone: <font color="Red"><?= $shipment['cons_telephone'] ?></font></td>
                            </tr>
                            <tr>
                                <td  class="tdtable2" colspan="2">Mobile:<font color="Red"><?= $shipment['cons_mobile'] ?></font></td>
                            </tr>
                        </table>


                    </td>
                </tr>

                <tr>
                    <td class="tdcontent tm5 rm5 lm5 b" valign="top">4.Packaging Type:  <font color=red>
                            <?php if ($shipment['package_type'] == "E"): ?>
Envelop
                            <?php elseif ($shipment['package_type'] == "M"): ?>
M Box
                            <?php elseif ($shipment['package_type'] == "L"): ?>
                            L Box
                            <?php else: ?>

                                others
                            <?php endif; ?>
                        </font>



                    </td>
                </tr>

                <tr>
                    <td class="tdcontent tm5 rm5 lm5 b" valign="top">5.Number Of Items:  <font color=red><?= $shipment['items_count'] ?></font>

                    </td>
                </tr>
                <tr>
                    <td class="tdcontent  tm5 bm3 rm5 lm5">
                        6.Goods Descriptions :- <font color='red'></font><br>
                        <table width="97%" border="0" cellspacing="0" cellpadding="0" class="tdtable" align="center">

                            <td rowspan="2" class="tdtable">Genral Description</td>
                            <td rowspan="2" class="tdtable">Number of Items</td>
                            <td class="tdtable" colspan="2">Weight</td>
                            <td colspan="3" class="tdtable">Dimension Vol&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cm</td>
                            </tr>
                            <tr>
                                <td class="tdtable">Kilos</td>
                                <td class="tdtable">Grams</td>
                                <td class="tdtable">Length</td>
                                <td class="tdtable">Width</td>
                                <td class="tdtable">Height</td>
                            </tr>
                            <tr>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                                <td class="tdtable">&nbsp;</td>
                            </tr>
                        </table>



                    </td>
                </tr>
            </table>




        </td>

        <td colspan="2" valign="top" class="tm10">
            <table width="400" border="0" cellspacing="3" cellpadding="0">
                <tr>
                    <td class="tdcontent rm5 lm5" height="40">
                        <center>
                            <table border="0" cellspacing="2" cellpadding="2" width="100%">


                                <tr>
                                    <td valign="top"><font size=4>Shipment Code  <?= $shipment['shipment_code']?></font><br />
                                        <font size=1><?= ConvertToOrcaleFormate($shipment['issue_datetime']);?></font><br />
                                        <font size="4"> <?=$City_Char?> </font></td>
                                    <td ><img src="<?= BARCODE ?>generateBarCode.php?code=<?=$code?>" width="120" height="60"  /></td>
                                </tr>
                            </table>
                        </center>
                        <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2">

                            <tr>
                                <td class="tdtable1">7. Services :</td>
                                <td align="left"><font color=red><?= $shipment['service_name'] ?></font></td>
                                <td>&nbsp;</td>
                                <td align="right" class="tdtable1"><span class="lm20 tdtable1">Insured</span> value</td>
                                <td>.....................</td>
                            </tr>
                            <tr>
                                <td colspan="4" height="10px;" ></td>

                            </tr>
                        </table>


                    </td>
                </tr>



                <tr>
                    <td class="tdcontent tm5 bm5 rm5 lm5" height="20">


                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="150" class="tdtable1">8. Method of Payment :- </td>
                                <td width="100"><font color='red'><?= $payment_method ?></font></td>
                                <td width="40" class="tdtable1"><i>Amount</i></td>
                                <td>.....................</td>
                            </tr>
                        </table>




                    </td>
                </tr>

                <tr>
                    <td class="tdcontent tm5 bm5 rm5 lm5">

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdtable1">9. Dangerous Goods</td>
                            </tr>
                            <tr>
                                <td height="10"><img src="images/spacer.gif" width="1" height="1" alt="" border="0" /></td>
                            </tr>
                            <tr>
                                <td class="lm15 tdtable1">Does This Consignment contain any dangerous Goods? &nbsp;<font color='red'><?= $is_dangerous_goods?></font></td>
                            </tr>
                        </table>


                    </td>
                </tr>
                <tr>
                    <td class="tdcontent tm5 bm5 rm5 lm5">

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2" class="bm5 tdtable1">10.Shipper`s Signature</td>

                            </tr>
                            <tr>
                                <td colspan="2" class="tdtable2">&nbsp;&nbsp;&nbsp;Name:</td>
                            </tr>

                            <tr>
                                <td class="tdtable2">&nbsp;&nbsp;&nbsp;&nbsp;Signature</td>
                                <td class="tdtable2">Date</td>
                            </tr>
                        </table>




                    </td>
                </tr>
                <tr>
                    <td class="tdcontent tm5 bm5 rm5 lm5">


                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2" class="bm5 tdtable1">11.Consignee`s Signature</td>

                            </tr>
                            <tr>
                                <td colspan="2" class="tdtable2">&nbsp;&nbsp;&nbsp;&nbsp;Name:</td>
                            </tr>

                            <tr>
                                <td class="tdtable2">&nbsp;&nbsp;&nbsp;&nbsp;Signature</td>
                                <td class="tdtable2">Date</td>
                            </tr>
                        </table>



                    </td>
                </tr>
                <tr>
                    <td class="tdcontent tm5 bm5 rm5 lm10">

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2" class="bm15 tdtable1">12.CLEX`s Signature</td>

                            </tr>
                            <tr>
                                <td class="tdtable2">&nbsp;&nbsp;&nbsp;&nbsp;Name:<br /></td>
                                <td class="tdtable2">Id</td>
                            </tr>

                            <tr>
                                <td class="tdtable2">&nbsp;&nbsp;&nbsp;&nbsp;Signature</td>
                                <td class="tdtable2">Date</td>
                            </tr>
                        </table>


                    </td>
                </tr>
                <tr>
                    <td  class="tdcontent tm5 bm5 rm5 lm10">


                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2" class="tdtable2">Notes:</td>

                            </tr>
                            <tr>
                                <td class="tdtable2" colspan="2"><font color='red'><?= $shipment['notes'] ?></font></td>
                            </tr>


                        </table>



                    </td>
                </tr>

            </table>


        </td>
        <td width="1"><img src="images/spacer.gif" width="1" height="1" alt=""  /></td>
    </tr>

</table>

<?php endfor; ?>
</body>
</html>
<script>
    window.print();
    </script>