<?php $this->load->view("header"); ?>

<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>

                <li>
                    <a href="#">Shipments</a>
                </li>
                <li class="active">Add Shipment</li>
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- /.nav-search -->
        </div>

        <div class="page-content">
            <div class="ace-settings-container" id="ace-settings-container">
                <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                    <i class="ace-icon fa fa-cog bigger-130"></i>
                </div>

                <div class="ace-settings-box clearfix" id="ace-settings-box">
                    <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <div class="pull-left">
                                <select id="skin-colorpicker" class="hide">
                                    <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                    <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                    <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                    <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                </select>
                            </div>
                            <span>&nbsp; Choose Skin</span>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                            <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                            <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                            <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                            <label class="lbl" for="ace-settings-add-container">
                                Inside
                                <b>.container</b>
                            </label>
                        </div>
                    </div><!-- /.pull-left -->

                    <div class="pull-left width-50">
                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                            <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                            <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                        </div>

                        <div class="ace-settings-item">
                            <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                            <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                        </div>
                    </div><!-- /.pull-left -->
                </div><!-- /.ace-settings-box -->
            </div><!-- /.ace-settings-container -->

            <div class="page-header">
                <h1>
                    Shipments
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Edit shipment status <br>     <span style="color: red"><?php if(isset($status)) echo $status ?> </span>
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <form class="form-horizontal" role="form" method="post">

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Code</label>
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> <?= $shipment['shipment_code'] ?></label>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Code</label>

                            <div class="col-sm-6">
                                <select name="status" class="form-control" id="form-field-select-3" required>
                                    <?php foreach ($statuses as $status): ?>
                                    <option value="<?=$status['id']?>" <?php if($shipment['status'] == $status['id'] ) echo "selected" ?>><?=$status['title'] ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Status notes </label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-2" name="status_notes" placeholder="Status notes" class="col-xs-10 col-sm-6" required/>

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Received Date </label>

                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input id="date-timepicker1" type="text" name="received_date" class="form-control col-xs-10 col-sm-6" required>
                                    <span class="input-group-addon">
																<i class="fa fa-clock-o bigger-110"></i>
															</span>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Consignee No </label>
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"><?= $shipment['cons_id'] ?> </label>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Consignee Name </label>

                            <div class="col-sm-9">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> <?= $shipment['cons_name'] ?> </label>


                            </div>
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Address </label>

                            <div class="col-sm-9">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-2"><?= $shipment['cons_address'] ?> </label>



                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> City </label>

                            <div class="col-sm-9">
                                <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> <?= $shipment['cons_city'] ?> </label>


                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-4">ZIP</label>

                            <div class="col-sm-9">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-4"> <?= $shipment['cons_zip'] ?></label>


                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-5">Contact Person</label>

                            <div class="col-sm-9">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-5"> <?= $shipment['cons_contact'] ?></label>


                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Telephone</label>

                            <div class="col-sm-9">
                                <label class="col-sm-3 control-label no-padding-right"> <?= $shipment['cons_telephone'] ?></label>


                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-6">Mobile</label>
                            <div class="col-sm-9">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-6"><?= $shipment['cons_mobile'] ?></label>


                            </div>
                        </div>

                        <div class="space-4"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">Package type</label>

                            <div class="col-sm-2">
                                <div class="inline">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">

                                    <?php
                                    if ($shipment['package_type'] == "M")
                                    {
                                        echo "M Box";
                                    }
                                    elseif ($shipment['package_type'] == "L")
                                    {
                                        echo "L Box";
                                    }
                                    elseif ($shipment['package_type'] == "E")
                                    {
                                        echo "Envelop";
                                    }
                                    elseif ($shipment['package_type'] == "O")
                                    {
                                        echo "Others";
                                    }
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-tags">Items Number</label>

                            <div class="col-sm-1">
                                <div class="inline">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-tags"> <?= $shipment['items_count'] ?></label>


                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-6">Service type</label>

                            <div class="col-sm-9">
                                <div class="inline">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-6"> <?= $shipment['service_type']['service_name'] ?></label>



                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-tags"> Payment</label>

                            <div class="col-sm-2">
                                <div class="inline">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">
                                        <?php
                                        if ($shipment['payment_method'] == "A")
                                        {
                                            echo "Account";
                                        }
                                        elseif ($shipment['payment_method'] == "C")
                                        {
                                            echo "Cash";
                                        }
                                        elseif ($shipment['payment_method'] == "D")
                                        {
                                            echo "COD";
                                        }

                                        ?>
                                    </label>
                                </div>
                            </div>
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-tags"> Dangerous Goods</label>

                            <div class="col-sm-1">
                                <div class="inline">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-tags">
                                        <?php
                                        if ($shipment['is_dangerous_good'] == 0)
                                        {
                                            echo "No";
                                        }
                                        elseif ($shipment['is_dangerous_good'] == 1)
                                        {
                                            echo "Yes";
                                        }


                                        ?>
                                    </label>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">  Date</label>

                            <div class="col-sm-9">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-tags">   <?= $shipment['issue_datetime'] ?></label>


                            </div>
                        </div>

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" name="submit" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>


                            </div>
                        </div>


                    </form>


                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<?php $this->load->view("footer"); ?> 
</body>
</html>
<script>

	$( "#form-field-select-3" ).change(function() {
		$.post( "<?= site_url()?>shipments/get_consignee_data", {consignee_id:  $( "#form-field-select-3" ).val()},function( data ) {
			var obj = jQuery.parseJSON(data);
            $("[name='address']").val(obj[0]['address']);
            $("[name='city']").val(obj[0]['city']);
            $("[name='zip']").val(obj[0]['zip']);
            $("[name='contact']").val(obj[0]['contact']);
            $("[name='tel']").val(obj[0]['telephone']);
            $("[name='mob']").val(obj[0]['mobile']);
            $('#form-field-select-4').val(obj[0]['consignee_id']);//To select Blue
            $('#form-field-select-4').trigger("chosen:updated");
//			var html = "<option value=''>  اختر المنطقة</option>";
//			$.each(obj, function( index, value ) {
//				html += "<option value='"+value['id']+"'>"+value['name']+"</option>";
//			});
//			$('#area_select_div').html(html);
		});
	});

    $( "#form-field-select-4" ).change(function() {
        $.post( "<?= site_url()?>shipments/get_consignee_data", {consignee_id: $( "#form-field-select-4" ).val()},function( data ) {
            var obj = jQuery.parseJSON(data);
            $("[name='address']").val(obj[0]['address']);
            $("[name='city']").val(obj[0]['city']);
            $("[name='zip']").val(obj[0]['zip']);
            $("[name='contact']").val(obj[0]['contact']);
            $("[name='tel']").val(obj[0]['telephone']);
            $("[name='mob']").val(obj[0]['mobile']);
            $('#form-field-select-3').val(obj[0]['consignee_id']+'_'+obj[0]['name']);//To select Blue
            $('#form-field-select-3').trigger("chosen:updated");
        });
    });

if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true});
    //resize the chosen on window resize


    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
        if(event_name != 'sidebar_collapsed') return;
        $('.chosen-select').each(function() {
            var $this = $(this);
            $this.next().css({'width': $this.parent().width()});
        })
    });


}

    if(!ace.vars['old_ie']) $('#date-timepicker1').datetimepicker({
        //format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-arrows ',
            clear: 'fa fa-trash',
            close: 'fa fa-times'
        }
    }).next().on(ace.click_event, function(){
        $(this).prev().focus();
    });


</script>