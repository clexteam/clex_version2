<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipments_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }


    public function get_active_consignees_by_name($name, $customer_id)
    {
        $this->db->where('is_active', 1);
        $this->db->like('consignee_name', $name);
        if ($customer_id != 0) $this->db->where('cust_acc_id', $customer_id);
        $query = $this->db->get('consignees', 20);
        return $query->result_array();
    }

    public function get_active_consignees_by_code($code, $customer_id)
    {
        $this->db->where('is_active', 1);
        $this->db->like('consignee_id', $code);
        if ($customer_id != 0) $this->db->where('cust_acc_id', $customer_id);
        $query = $this->db->get('consignees', 20);
        return $query->result_array();
    }

	public function get_consignee_by_id($id)
	{
		$this->db->where('consignee_id', $id);
		$this->db->from('consignees');
        $this->db->join('cities', 'consignees.city_id = cities.city_id', 'left');
        $query = $this->db->get();
        return $query->result_array();
	}

    public function get_consignee_by_name($name)
	{
		$this->db->where('consignee_name', $name);
		$this->db->from('consignees');
        $this->db->join('cities', 'consignees.city_id = cities.city_id', 'left');
        $query = $this->db->get();
        return $query->result_array();
	}

    public function shipments_count()
    {
        //$this->db->where('consignee_id', $id);
        $this->db->order_by('shipment_code', 'desc');
        $query = $this->db->get('shipments');
        return $query->num_rows();
    }

	public function get_all($limit, $offset)
	{
		$this->db->order_by("shipment_code", 'desc');
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
		$query = $this->db->get('shipments', $limit, $offset);
		return $query->result_array();
	}

    public function get_shipment_for_print($shipment_id)
    {
        $this->db->where("shipment_code", $shipment_id);
        $this->db->join('customers', 'shipments.cust_acc_id = customers.customer_id', 'left');
        $this->db->join('cities', 'customers.city_id = cities.city_id', 'left');
        $this->db->join('services', 'shipments.service_type = services.service_id', 'left');
        $query = $this->db->get('shipments');
        return $query->row_array();
    }

    public function get_shipment_by_id($id)
    {
        $this->db->where('shipment_code', $id);
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $query = $this->db->get('shipments');
        return $query->result_array();
    }

    public function get_bulk_shipments($bulk_codes, $limit, $offset)
    {
        $this->db->where("shipment_code IN ($bulk_codes)");
        $this->db->join('statuses', 'shipments.status = statuses.id', 'left');
        $query = $this->db->get('shipments', $limit, $offset);
        return $query->result_array();
    }

    public function get_shipment_movements($id)
    {
        $this->db->join('users', 'shipment_movements.changed_by = users.id', 'left');
        $this->db->where("shipment_movements.shipment_code", $id);
        $query = $this->db->get('shipment_movements');
        return $query->result_array();
    }

    public function insert_shipment($cons_name, $cons_code, $cust_id, $address, $city, $zip, $contact, $tel, $mob,
                                    $service_type, $package_type, $goods_desc, $payment, $is_dangerous, $notes, $number, $user_id)
    {
       $this->db->insert('shipments', array('cons_id' => $cons_code, 'cust_acc_id' => $cust_id, 'cons_name' => $cons_name ,'cons_address' => $address, 'cons_city' => $city, 'cons_zip' => $zip, 'cons_contact' => $contact,
			   'cons_telephone' => $tel, 'cons_mobile' => $mob, 'package_type' => $package_type, 'service_type' => $service_type, 'goods_description' =>$goods_desc, 'payment_method' =>$payment,
               'is_dangerous_good'=> $is_dangerous, 'notes' => $notes, 'status' => 1, 'items_count'=> $number, 'created_by' => $user_id));
       return $this->db->insert_id();
    }

    public function add_to_movement_history($id, $from_status, $to_status, $changed_by)
    {
        $this->db->insert('shipment_movements', array('shipment_code' => $id, 'from_status' => $from_status ,'to_status' => $to_status, 'change_time' => date("Y-m-d H:i:s"), 'changed_by' => $changed_by));

    }

    public function update_shipment_status($id, $status, $note, $received_date)
    {
        $this->db->where("shipment_code", $id);

        $this->db->update('shipments', array('status' => $status, 'status_notes' => $note, 'received_at' => $received_date));

    }

	public function edit_doctor($id, $name, $gender, $title, $section, $city, $area, $address, $phone, $work_time, $price, $describtion, $picture_name)
	{
		$this->db->where("id", $id);
		if (!empty($picture_name))
		{
			$this->db->update('doctors', array('name' => $name, 'gender' => $gender, 'title' => $title, 'section_id' => $section, 'city_id' => $city, 'area_id' => $area,
					'address' => $address, 'phone' => $phone, 'working_time' => $work_time, 'price' => $price, 'description' => $describtion, 'image' => $picture_name));
		}
		else
		{
			$this->db->update('doctors', array('name' => $name, 'gender' => $gender, 'title' => $title, 'section_id' => $section, 'city_id' => $city, 'area_id' => $area,
					'address' => $address, 'phone' => $phone, 'working_time' => $work_time, 'description' => $describtion, 'price' => $price));

		}

	}

    

	

}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */