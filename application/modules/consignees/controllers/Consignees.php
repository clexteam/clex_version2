<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consignees extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("consignees_model");
    }
	
	
	public function index()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
			$current_page = (int) $this->uri->segment(2);
			$per_page = 10;
			$users_count = $this->consignees_model->get_consignees_count();
			$config["base_url"] = site_url() . "consignees/";
			$config['uri_segment'] = 2;
			$config["total_rows"] = $users_count;
			$config["per_page"] = $per_page;

            //config for bootstrap pagination class integration
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			$users = $this->consignees_model->get_all($per_page, $current_page);
			if (isset($_POST['search']) AND !empty($_POST['search']))
			{
                redirect(site_url() . "consignees_search/".htmlspecialchars(trim($_POST['search'])));
            }

			if ($users)
			{
				$data["shipments"] = $users;
			}
			
			$this->load->view("manage_consignees_view", $data);
//		}
	}

    public function upload()
    {
        $data = array();
        $data["status"] = "";
        $data['customers'] = $this->consignees_model->get_active_customers();
        if (isset($_POST["submit"]))
        {
            $customer = $_POST['customer'];
            $file_name = "CONS_".date("YmdHis")."_".$_FILES["file"]["name"];
            $file_parts = @pathinfo($file_name);
            $accepted_extension = Array('csv');

            if (in_array($file_parts['extension'], $accepted_extension))
            {
                move_uploaded_file($_FILES["file"]["tmp_name"],CONSIGNEES_PATH.$file_name);
                chmod(CONSIGNEES_PATH.$file_name,0777);
                $file_handle = fopen(CONSIGNEES_PATH.$file_name, "r");
                $Counter=0;
                while (!feof($file_handle) )
                {
                    $line_of_text = fgetcsv($file_handle);
                    if (!empty($line_of_text))
                    {

                        for ($i = 0; sizeof($line_of_text) > $i; $i++)
                        {
                            switch ($i) {
                                case 0:
                                    $CONS_NO = addslashes($line_of_text[$i]);
                                    break;
                                case 1:
                                    $CONS_NAME = addslashes($line_of_text[$i]);
                                    break;
                                case 2:
                                    $Region = addslashes($line_of_text[$i]);
                                case 3:
                                    $CONS_CITY = addslashes($line_of_text[$i]);
                                    break;
                                case 4:
                                    $CONS_ADDRESS1 = addslashes($line_of_text[$i]);
                                    break;
                                case 5:
                                    $CONS_ADDRESS2 = addslashes($line_of_text[$i]);
                                    break;
                                case 6:
                                    $CONS_ADDRESS3 = addslashes($line_of_text[$i]);
                                    break;
                                case 7:
                                    $CONS_ADDRESS4 = addslashes($line_of_text[$i]);
                                    break;
                                case 8:
                                    $CONS_CONTACT = addslashes($line_of_text[$i]);
                                    break;
                                case 9:
                                    $CONS_TEL1 = addslashes($line_of_text[$i]);
                                    break;
                                case 10:
                                    $CONS_TEL2 = addslashes($line_of_text[$i]);
                                    break;
                                case 11:
                                    $CONS_MOB = addslashes($line_of_text[$i]);
                                    break;
                                default:
                                    break;
                            }
                        }

                        if ($Counter == 0)
                        {
                            //i am checking fileds names
                            $AlertMessage = "";
                            if ($CONS_NO != "Customer ID") {
                                $AlertMessage .= "Error in Naming  Filed ((Customer ID)), It is NOT ((" . $CONS_NO . "))\\n";
                            }

                            if ($CONS_NAME != "Customer Name") {
                                $AlertMessage .= "Error in Naming  Filed ((Customer Name)), It is NOT ((" . $CONS_NAME . "))\\n";
                            }

                            if ($Region != "Region") {
                                $AlertMessage .= "Error in Naming  Filed ((Region)), It is NOT ((" . $Region . "))\\n";
                            }


                            if ($CONS_CITY != "District") {
                                $AlertMessage .= "Error in Naming  Filed ((District)), It is NOT ((" . $CONS_CITY . "))\\n";
                            }

                            if ($CONS_ADDRESS1 != "Address 1") {
                                $AlertMessage .= "Error in Naming  Filed ((Address 1)), It is NOT ((" . $CONS_ADDRESS1 . "))\\n";
                            }

                            if ($CONS_ADDRESS2 != "Address 2") {
                                $AlertMessage .= "Error in Naming  Filed ((Address 2)), It is NOT ((" . $CONS_ADDRESS2 . "))\\n";
                            }


                            if ($CONS_ADDRESS3 != "Address 3") {
                                $AlertMessage .= "Error in Naming  Filed ((Address 3)), It is NOT ((" . $CONS_ADDRESS3 . "))\\n";
                            }

                            if ($CONS_ADDRESS4 != "Address 4") {
                                $AlertMessage .= "Error in Naming  Filed ((Address 4)), It is NOT ((" . $CONS_ADDRESS4 . "))\\n";
                            }

                            if ($CONS_CONTACT != "Contact Person") {
                                $AlertMessage .= "Error in Naming  Filed ((Contact Person)), It is NOT ((" . $CONS_CONTACT . "))\\n";
                            }

                            if ($CONS_TEL1 != "Phone No1") {
                                $AlertMessage .= "Error in Naming  Filed ((Phone No1)), It is NOT ((" . $CONS_TEL1 . "))\\n";
                            }

                            if ($CONS_TEL2 != "Phone No2") {
                                $AlertMessage .= "Error in Naming  Filed ((Phone No2)), It is NOT ((" . $CONS_TEL2 . "))\\n";
                            }

                            if ($CONS_MOB != "Phone No3") {
                                $AlertMessage .= "Error in Naming  Filed ((Phone No3)), It is NOT ((" . $CONS_MOB . "))\\n";
                            }

                            if($AlertMessage!="")
                            {
                                $data["status"].= "<p class='error-msg'>$AlertMessage</p>";
                            }
                            else
                            {
                                $this->consignees_model->delete_old_consignees($customer);

                            }

                        }
                        else
                        {
                            if($CONS_NO!="")
                            {
                                $city = $this->consignees_model->get_city_by_name($CONS_CITY);
                                $CONS_NAME = mb_convert_encoding($CONS_NAME, "HTML-ENTITIES", "UTF-8");
                                $IS_ACTIVE = 1;
                                $REMARKS = "";
                                $CONS_ADDRESS = $CONS_ADDRESS1."-".$CONS_ADDRESS2."-".$CONS_ADDRESS3."-".$CONS_ADDRESS4;
                                $CONS_TEL = $CONS_TEL1."-".$CONS_TEL2;
                                $inserted_id = $this->consignees_model->insert_consignee($CONS_NO, $CONS_NAME, 0, $customer, $city['city_id'], null, null, $CONS_ADDRESS,
                                    null, $CONS_CONTACT, $CONS_TEL, $CONS_MOB, $REMARKS, $IS_ACTIVE);
                                if ($inserted_id > 0)
                                {
                                    @$data["status"].= "<p class='error-msg'>$inserted_id  Inserted !</p><br>";
                                }
                                else
                                {
                                    $cons_count = $this->consignees_model->check_is_consignee_exits($CONS_NO, $customer);
                                    if ($cons_count > 0)
                                    {
                                        $data["status"].= "<p class='error-msg'>$CONS_NO  Repeated !</p><br>";
                                    }
                                }

                            }
                        }

                        $Counter++;
                    }
                }
            }
            else
            {
                $data["status"].= "<p class='error-msg'>Only CSV files extension accepted!</p>";
            }

        }
        $this->load->view("upload_consignees_view", $data);

    }


    public function add()
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
			$data['customers'] = $this->consignees_model->get_active_customers();
            $data['cities'] = $this->common_model->get_all_from_table("cities");

			if (isset($_POST["submit"]))
			{
                $code = htmlspecialchars(trim($_POST["code"]));
                $name = htmlspecialchars(trim($_POST["name"]));
                $type = $_POST['type'];
                if ($this->session->userdata("customer") !=0) $customer = $this->session->userdata("customer"); else $customer = $_POST['customer'];
                $city = $_POST['city'];
                $district = htmlspecialchars(trim($_POST["district"]));
                $street = htmlspecialchars(trim($_POST["street"]));
                $address = htmlspecialchars(trim($_POST["address"]));
                $zip = htmlspecialchars(trim($_POST["zip"]));
                $contact = htmlspecialchars(trim($_POST["contact"]));
                $tel = htmlspecialchars(trim($_POST["tel"]));
                $mob = @htmlspecialchars(trim($_POST["mob"]));
                $remarks = htmlspecialchars(trim($_POST["remarks"]));

                if (isset($_POST['active'])) $is_active = 1; else $is_active = 0;

                $data['status'] = "";

				if (empty($code) OR empty($name) OR empty($customer) OR empty($city) OR empty($address) OR empty($tel))
				{
					$data["status"].= "<p class='error-msg'> Please fill in all required fields!</p>";
				}

				else
				{
                    $insert_id = $this->consignees_model->insert_consignee($code, $name, $type, $customer, $city, $district, $street, $address,
                        $zip, $contact, $tel, $mob, $remarks, $is_active);
                    $this->session->set_flashdata("status", "Consignee added successfully.");
                    redirect(site_url() . "consignees");
				}
			}
			
			$this->load->view("add_consignee_view", $data);
	//	}
	}

    public function search($search_keyword = "")
    {
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
        if (!$search_keyword) show_404();
        $search_keyword = htmlspecialchars_decode($search_keyword);
        $data = array();
        $current_page = (int) $this->uri->segment(3);
        $per_page = 10;
        $config["base_url"] = site_url() . "consignees_search/$search_keyword";
        $config['uri_segment'] = 3;
        $config["total_rows"] = $this->consignees_model->get_consignees_search_count($search_keyword);
        $config["per_page"] = $per_page;
        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
        $users = $this->consignees_model->get_all_by_keyword($search_keyword, $per_page, $current_page);

        if (isset($_POST['search']) AND !empty($_POST['search']))
        {
            redirect(site_url() . "consignees_search/".htmlspecialchars(trim($_POST['search'])));
        }

        if ($users)
        {
            $data["shipments"] = $users;
        }

        $this->load->view("manage_consignees_view", $data);
//		}
    }
	
	public function edit($id = "")
	{
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$data = array();
            $data['user'] = $this->common_model->get_subject_with_token("consignees", "consignee_id", $id);
            if (!$id OR !$data['user']) show_404();
			$data['cities'] = $this->common_model->get_all_from_table("cities");
            $data['customers'] = $this->consignees_model->get_active_customers();

            if (isset($_POST["submit"]))
            {
                $code = htmlspecialchars(trim($_POST["code"]));
                $name = htmlspecialchars(trim($_POST["name"]));
                $type = $_POST['type'];
                if ($this->session->userdata("customer") !=0) $customer = $this->session->userdata("customer"); else $customer = $_POST['customer'];
                $city = $_POST['city'];
                $district = htmlspecialchars(trim($_POST["district"]));
                $street = htmlspecialchars(trim($_POST["street"]));
                $address = htmlspecialchars(trim($_POST["address"]));
                $zip = htmlspecialchars(trim($_POST["zip"]));
                $contact = htmlspecialchars(trim($_POST["contact"]));
                $tel = htmlspecialchars(trim($_POST["tel"]));
                $mob = @htmlspecialchars(trim($_POST["mob"]));
                $remarks = htmlspecialchars(trim($_POST["remarks"]));

                if (isset($_POST['active'])) $is_active = 1; else $is_active = 0;

                $data['status'] = "";

                if (empty($code) OR empty($name) OR empty($customer) OR empty($city) OR empty($address) OR empty($tel))
                {
                    $data["status"].= "<p class='error-msg'> Please fill in all required fields!</p>";
                }

                else
                {
                    $insert_id = $this->consignees_model->update_consignee($id, $code, $name, $type, $customer, $city, $district, $street, $address,
                        $zip, $contact, $tel, $mob, $remarks, $is_active);
                    $this->session->set_flashdata("status", "Consignee updated successfully.");
                    redirect(site_url() . "consignees");
                }
            }

        $this->load->view("edit_consignee_view", $data);
//		}
	}
	
	
	public function delete($id = "")
    {	
//		$authorized = $this->common_model->authorized_to_view_page("doctor_permissions");
//		if ($authorized)
//		{
			$user = $this->common_model->get_subject_with_token("users", "id", $id);
			if (empty($id) OR ! $user) redirect(site_url() . "users");
		
			$this->common_model->delete_subject("users", "id", $id);
			
			$this->session->set_flashdata("status", "User deleted successfully!");
			redirect($_SERVER['HTTP_REFERER']);
//		}
	}
	


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */