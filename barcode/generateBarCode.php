<?php

include('BarcodeGenerator.php');
include('BarcodeGeneratorPNG.php');
include('BarcodeGeneratorSVG.php');
include('BarcodeGeneratorJPG.php');
include('BarcodeGeneratorHTML.php');
header('Content-Type: image/png');
$code = intval($_GET['code']);
$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
echo $generator->getBarcode($code, $generator::TYPE_CODE_128_A);