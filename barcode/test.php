<?php

include('BarcodeGenerator.php');
include('BarcodeGeneratorPNG.php');
include('BarcodeGeneratorSVG.php');
include('BarcodeGeneratorJPG.php');
include('BarcodeGeneratorHTML.php');


$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
echo $generator->getBarcode('123', $generator::TYPE_CODE_128_A);

$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode('123', $generator::TYPE_CODE_128_A)) . '">';